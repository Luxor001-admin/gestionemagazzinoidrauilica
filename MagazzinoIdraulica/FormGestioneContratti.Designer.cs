﻿namespace MagazzinoIdraulica
{
    partial class FormGestioneContratti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datagrid_contratti = new System.Windows.Forms.DataGridView();
            this.Tipologia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PagaOraria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_contratti)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(428, 27);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvaToolStripMenuItem,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 23);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // salvaToolStripMenuItem
            // 
            this.salvaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.salva;
            this.salvaToolStripMenuItem.Name = "salvaToolStripMenuItem";
            this.salvaToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.salvaToolStripMenuItem.Text = "Salva";
            this.salvaToolStripMenuItem.Click += new System.EventHandler(this.Salva);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(109, 24);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.Esci);
            // 
            // datagrid_contratti
            // 
            this.datagrid_contratti.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.datagrid_contratti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagrid_contratti.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipologia,
            this.PagaOraria});
            this.datagrid_contratti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagrid_contratti.EnableHeadersVisualStyles = false;
            this.datagrid_contratti.Location = new System.Drawing.Point(0, 27);
            this.datagrid_contratti.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.datagrid_contratti.Name = "datagrid_contratti";
            this.datagrid_contratti.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.datagrid_contratti.RowHeadersWidth = 40;
            this.datagrid_contratti.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.datagrid_contratti.RowTemplate.Height = 24;
            this.datagrid_contratti.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.datagrid_contratti.Size = new System.Drawing.Size(428, 342);
            this.datagrid_contratti.TabIndex = 4;
            this.datagrid_contratti.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagrid_contratti_modified);
            this.datagrid_contratti.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ConvalidaCella);
            this.datagrid_contratti.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.datagrid_contratti_EditingControlShowing);
            this.datagrid_contratti.SelectionChanged += new System.EventHandler(this.datagrid_contratti_SelectionChanged);
            this.datagrid_contratti.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.ContrattoEliminato);
            // 
            // Tipologia
            // 
            this.Tipologia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Tipologia.FillWeight = 70F;
            this.Tipologia.HeaderText = "Tipologia";
            this.Tipologia.MinimumWidth = 6;
            this.Tipologia.Name = "Tipologia";
            // 
            // PagaOraria
            // 
            this.PagaOraria.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "0,00 €";
            this.PagaOraria.DefaultCellStyle = dataGridViewCellStyle1;
            this.PagaOraria.FillWeight = 30F;
            this.PagaOraria.HeaderText = "Paga Oraria";
            this.PagaOraria.Name = "PagaOraria";
            // 
            // FormGestioneContratti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 369);
            this.Controls.Add(this.datagrid_contratti);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormGestioneContratti";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestione Contratti";
            this.Load += new System.EventHandler(this.GestioneContratti_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_contratti)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.DataGridView datagrid_contratti;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipologia;
        private System.Windows.Forms.DataGridViewTextBoxColumn PagaOraria;
    }
}