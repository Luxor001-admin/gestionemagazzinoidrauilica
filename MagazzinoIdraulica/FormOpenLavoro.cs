﻿using MagazzinoIdraulica.Code;
using MagazzinoIdraulicaDB.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormOpenLavoro : Form
    {
        dbContext dbContext;
        Lavoro lavoro;
        bool nuovoLavoro;
        List<MaterialeLavoro> materialiLavoroEliminati = new List<MaterialeLavoro>();
        List<Prestazione> prestazioniLavoroEliminate = new List<Prestazione>();
        public FormOpenLavoro(dbContext dbContext, Lavoro lavoro)
        {
            this.dbContext = dbContext;
            this.lavoro = lavoro;
            this.nuovoLavoro = !dbContext.Lavoro.Any(l => l.Id == lavoro.Id);
            this.materialiCacheati = dbContext.Materiale.ToList();
            this.contrattiCacheati = dbContext.TipoContratto.ToList();
            InitializeComponent();
        }

        private void MostraLavoro_Load(object sender, EventArgs e)
        {
            this.popolaDataGrid(this.lavoro);
            this.aggiornaTotali();
            this.chk_stampato.Checked = this.lavoro.Segnato;
        }

        private List<Materiale> materialiCacheati;
        private List<TipoContratto> contrattiCacheati;
        // Filtriamo i materiali rimanenti per l'autocomplete
        private void datagrid_materiali_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (datagrid_materiali.CurrentCell.ColumnIndex == 0)
            {
                TextBox autoText = e.Control as TextBox;
                if (autoText != null)
                {
                    autoText.AutoCompleteMode = AutoCompleteMode.Suggest;
                    autoText.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection source = new AutoCompleteStringCollection();

                    var materialiGiaSelezioanti = datagrid_materiali.Rows.Cast<DataGridViewRow>()
                    .Where(row => !row.IsNewRow)
                    .Where(row => row.Cells[0].Value != null)
                    .Select(rowMaterialeLavoro => { 
                        if (rowMaterialeLavoro.Tag != null)
                            return (rowMaterialeLavoro.Tag as MaterialeLavoro).IdMaterialeNavigation;
                        string valoreCella = rowMaterialeLavoro.Cells[0].Value.ToString();
                        return materialiCacheati.Find(materiale => materiale.Descrizione == valoreCella || materiale.Abbreviazione == valoreCella);
                    });
                    var descrizioniMateriali = materialiCacheati.Except(materialiGiaSelezioanti).Select(materiale => materiale.Descrizione).ToList();
                    var abbreviazioniMateriali = materialiCacheati.Except(materialiGiaSelezioanti).Where(materiale => !string.IsNullOrEmpty(materiale.Abbreviazione)).Select(materiale => materiale.Abbreviazione).ToList();
                    descrizioniMateriali.AddRange(abbreviazioniMateriali);

                    source.AddRange(descrizioniMateriali.ToArray());
                    autoText.AutoCompleteCustomSource = source;
                }
            }

            if (this.datagrid_materiali.CurrentCell.ColumnIndex != 1)
                return;
            e.Control.KeyPress += (value, a) =>
            {
                if (datagrid_materiali.CurrentCell.ColumnIndex == 1 && a.KeyChar == '.')
                    a.KeyChar = ',';
            };
        }

        // Filtriamo i contratti rimanenti per l'autocomplete
        private void datagrid_manodopera_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            string titleText = datagrid_prestazioni.Columns[0].HeaderText;
            if (titleText.Equals("Tipo"))
            {
                TextBox autoText = e.Control as TextBox;
                if (autoText != null)
                {
                    autoText.AutoCompleteMode = AutoCompleteMode.Suggest;
                    autoText.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection source = new AutoCompleteStringCollection();

                    var contratti = contrattiCacheati.Select(contratto => contratto.Nome).ToList();
                    source.AddRange(contratti.ToArray());
                    autoText.AutoCompleteCustomSource = source;
                }
            }
        }

        private void popolaDataGrid(Lavoro lavoro)
        {
            cmb_tipologie.Items.AddRange(dbContext.TipologiaLavoro.ToArray());
            this.datapicker_data.Value = lavoro.getData();
            this.cmb_tipologie.SelectedItem = this.nuovoLavoro && cmb_tipologie.Items.Count >= 1 ? cmb_tipologie.Items[0] : lavoro.IdTipologiaNavigation;
            this.txt_indirizzo.Text = lavoro.Indirizzo;
            this.txt_referente.Text = lavoro.Referente;
            this.txt_note.Text = lavoro.Note;
            var righeMateriali = lavoro.MaterialeLavoro.Select(materialeLavoro =>
            {
                DataGridViewRow row = (DataGridViewRow)datagrid_materiali.RowTemplate.Clone();
                row.CreateCells(datagrid_materiali);
                row.Cells[0].Value = materialeLavoro.IdMaterialeNavigation.Descrizione;
                row.Cells[1].Value = materialeLavoro.Quantita.ToString();
                row.Tag = materialeLavoro;
                return row;
            }).ToArray();
            this.datagrid_materiali.Rows.AddRange(righeMateriali);

            var righePrestazione = lavoro.Prestazione.Select(prestazione =>
            {
                DataGridViewRow row = (DataGridViewRow)datagrid_prestazioni.RowTemplate.Clone();
                row.CreateCells(datagrid_prestazioni);
                row.Cells[0].Value = prestazione.IdContrattoNavigation.Nome;
                row.Cells[1].Value = (int)TimeSpan.FromMinutes(prestazione.Tempo).TotalHours; // faccio un cast a int per rimuovere parti decimali di totalHours. 10 ore e mezza(630min) sono 10.5 TotalHours, che potrebbe essere mostrato come 11 nella cellvalue perché fà un arrotondamento!
                row.Cells[2].Value = TimeSpan.FromMinutes(prestazione.Tempo).Minutes;
                row.Tag = prestazione;
                return row;
            }).ToArray();

            this.datagrid_prestazioni.Rows.AddRange(righePrestazione);
        }

        private void aggiornaTotali()
        {
            decimal totaleMateriali = 0;
            decimal totaleManodopera = 0;
            var materiali = datagrid_materiali.Rows.Cast<DataGridViewRow>()
                .Where(row => row.Cells["Materiale"].FormattedValue.ToString() != string.Empty && materialiCacheati.Any(m => m.Descrizione == row.Cells["Materiale"].FormattedValue.ToString() || m.Abbreviazione == row.Cells["Materiale"].FormattedValue.ToString()) && row.Cells["Quantita"].FormattedValue.ToString() != String.Empty)
                .Select(row =>
                {
                    Materiale materiale = materialiCacheati.First(materiale => row.Cells["Materiale"].FormattedValue.ToString() == materiale.Descrizione || row.Cells["Materiale"].FormattedValue.ToString() == materiale.Abbreviazione);
                    return new MaterialeUsato(materiale, row.Cells["Quantita"].ValueToDecimal());
                });
            totaleMateriali = materiali.Select(materialeQuantita => materialeQuantita.materiale.Prezzo * (materialeQuantita.quantita)).Sum();

            var manodopere = datagrid_prestazioni.Rows.Cast<DataGridViewRow>()
                .Where(row => contrattiCacheati.Any(tipoContratto => tipoContratto.Nome == row.Cells["Tipo"].FormattedValue.ToString() && (row.Cells["Ore"].FormattedValue.ToString() != String.Empty || row.Cells["Minuti"].FormattedValue.ToString() != String.Empty)))
                .Select(row => {
                    TipoContratto contratto = contrattiCacheati.First(tipoContratto => tipoContratto.Nome == row.Cells["Tipo"].FormattedValue.ToString());
                    int minuti = row.Cells["Ore"].ValueToInt() * 60 + row.Cells["Minuti"].ValueToInt();
                    return new PrestazionePrevista(contratto, minuti);
                });
            totaleManodopera = manodopere.Select(manodopera => manodopera.calcolaPaga()).Sum();
            txt_fornitura.Text = Utilities.GetImportoCurrency(totaleMateriali);
            txt_manodopera.Text = Utilities.GetImportoCurrency(totaleManodopera);
            txt_totale.Text = Utilities.GetImportoCurrency(totaleMateriali + totaleManodopera);
        }

        private void Salva(object sender, EventArgs e)
        {
            this.lavoro.IdTipologia = (cmb_tipologie.SelectedItem as TipologiaLavoro).Id;
            this.lavoro.Data = BitConverter.GetBytes(datapicker_data.Value.Ticks);
            this.lavoro.Indirizzo = txt_indirizzo.Text;
            this.lavoro.Referente = txt_referente.Text;
            this.lavoro.Note = txt_note.Text;

            // se il lavoro non è presente nel db, allora significa che è nuovo
            if (nuovoLavoro)
                dbContext.Lavoro.Add(lavoro); // in tal caso lo aggiungo al db
            else
                dbContext.Entry(this.lavoro).CurrentValues.SetValues(this.lavoro); // altrimenti lo aggiorno
            dbContext.SaveChanges();

            List<MaterialeLavoro> materialiLavoro = this.GetMaterialiLavoroCorrenti();
            materialiLavoro.ForEach(materiale =>
            {
                if (this.dbContext.Exists(materiale))
                    dbContext.Entry(materiale).CurrentValues.SetValues(materiale);
                else
                    dbContext.MaterialeLavoro.Add(materiale);
            });

            List<Prestazione> prestazioniLavoro = this.GetPrestazioniCorrenti();
            prestazioniLavoro.ForEach(prestazioneLavoro =>
            {
                if (this.dbContext.Exists(prestazioneLavoro))
                    dbContext.Entry(prestazioneLavoro).CurrentValues.SetValues(prestazioneLavoro);
                else
                    dbContext.Prestazione.Add(prestazioneLavoro);
            });

            dbContext.Prestazione.RemoveRange(this.prestazioniLavoroEliminate);
            dbContext.MaterialeLavoro.RemoveRange(this.materialiLavoroEliminati);

            dbContext.SaveChanges();
            this.Close();
        }

        private void MaterialeLavoroEliminato(object sender, DataGridViewRowEventArgs e)
        {
            if(e.Row.Tag != null)
                this.materialiLavoroEliminati.Add(e.Row.Tag as MaterialeLavoro);
            aggiornaTotali();
        }

        private void PrestazioneLavoroEliminata(object sender, DataGridViewRowEventArgs e)
        {
            if (e.Row.Tag != null)
                this.prestazioniLavoroEliminate.Add(e.Row.Tag as Prestazione);
            aggiornaTotali();
        }

        private void Esci(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Stampa(object sender, EventArgs e)
        {
            Lavoro lavoroCache = new Lavoro();
            lavoroCache.IdCantiereNavigation = this.lavoro.IdCantiereNavigation;
            lavoroCache.IdTipologiaNavigation = this.lavoro.IdTipologiaNavigation;
            lavoroCache.Indirizzo = this.lavoro.Indirizzo;
            lavoroCache.MaterialeLavoro = GetMaterialiLavoroCorrenti();
            lavoroCache.Prestazione = GetPrestazioniCorrenti();
            lavoroCache.Referente = this.lavoro.Referente;
            lavoroCache.Data = this.lavoro.Data;
            FormStampaReport stampaReport = new FormStampaReport(this.dbContext, lavoroCache);
            stampaReport.ShowDialog();
        }

        private void MostraListino(object sender, EventArgs e)
        {
            FormGestioneListino formGestioneListino = new FormGestioneListino(this.dbContext, true);
            this.AddOwnedForm(formGestioneListino);
            formGestioneListino.Show();
            formGestioneListino.StartPosition = FormStartPosition.Manual;
            formGestioneListino.Location = new Point(this.Location.X + this.Size.Width, this.Location.Y);
        }

        private void ConvalidaCella(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if(sender == datagrid_materiali)
            {
                // trattasi della quantità: convalidiamo solo se contiene effettivamente stringhe
                if (datagrid_materiali.Columns[e.ColumnIndex].Name == "Quantita")
                    datagrid_materiali.ValidateDecimalCell(e);
            }

            if(sender == datagrid_prestazioni)
            {
                // trattasi della quantità: convalidiamo solo se contiene effettivamente stringhe
                if (datagrid_prestazioni.Columns[e.ColumnIndex].Name == "Ore")
                    datagrid_prestazioni.ValidateIntCell(e, 0);
                // trattasi della quantità: convalidiamo solo se contiene effettivamente stringhe
                if (datagrid_prestazioni.Columns[e.ColumnIndex].Name == "Minuti")
                    datagrid_prestazioni.ValidateIntCell(e, 0);
            }
        }

        private List<MaterialeLavoro> GetMaterialiLavoroCorrenti()
        {
            var materiali = datagrid_materiali.Rows.Cast<DataGridViewRow>()
                .Where(row => !row.IsNewRow)
                .Where(row =>
                {
                    string valoreCella = row.Cells["Materiale"].EditedFormattedValue.ToString();
                    return dbContext.Materiale.Any(materiale => materiale.Descrizione == valoreCella || materiale.Abbreviazione == valoreCella) && row.Cells["Quantita"].ValueToDecimal() > 0;
                })
                .Select(rowMaterialeLavoro =>
                {
                    // individuiamo il materiale associato alla descrizione
                    string valoreCella = rowMaterialeLavoro.Cells["Materiale"].EditedFormattedValue.ToString();
                    MaterialeLavoro materialeLavoro = (MaterialeLavoro)rowMaterialeLavoro.Tag ?? new MaterialeLavoro();
                    Materiale materialeAssociato = dbContext.Materiale.ToList().Find(materiale => materiale.Descrizione == valoreCella || materiale.Abbreviazione == valoreCella);

                    materialeLavoro.IdMaterialeNavigation = materialeAssociato;
                    materialeLavoro.IdLavoroNavigation = this.lavoro;
                    materialeLavoro.Quantita = rowMaterialeLavoro.Cells["Quantita"].ValueToDecimal();                    
                    return materialeLavoro;
                }).ToList();
            return materiali;
        }

        private List<Prestazione> GetPrestazioniCorrenti()
        {
            var prestazioni = datagrid_prestazioni.Rows.Cast<DataGridViewRow>()
                  .Where(row => !row.IsNewRow)
                  .Where(row =>
                  {
                      string valoreCella = row.Cells["Tipo"].EditedFormattedValue.ToString();
                      return contrattiCacheati.Any(contratto => contratto.Nome == valoreCella); // VOLONTARIAMENTE non faccio il controllo se il valore minuti/ore è > 0, in quanto prevedo che sia possibile associare al lavoro una prestazione senza indicare un orario. della serie: "Si ci ho lavorato, non so per quanto"
                  })
                  .Select(rowPrestazioniLavoro =>
                  {
                      string valoreCella = rowPrestazioniLavoro.Cells["Tipo"].EditedFormattedValue.ToString();
                      Prestazione prestazioneLavoro = (Prestazione)rowPrestazioniLavoro.Tag ?? new Prestazione();
                      TipoContratto tipoContrattoAssociato = contrattiCacheati.ToList().Find(tipoContratto => tipoContratto.Nome == valoreCella);

                      prestazioneLavoro.IdContrattoNavigation = tipoContrattoAssociato;
                      prestazioneLavoro.Tempo = rowPrestazioniLavoro.Cells["Ore"].ValueToInt() * 60 + rowPrestazioniLavoro.Cells["Minuti"].ValueToInt();
                      prestazioneLavoro.IdLavoroNavigation = this.lavoro;                      
                      return prestazioneLavoro;
                  }).ToList();
            return prestazioni;
        }

        // GESTIONE "FOCUS ALLA CELLA ADIACENTE" QUANDO SI INSERISCE UNA CELLA 
        private DataGridViewCell ultimaCellaEditata;
        // copiato da https://stackoverflow.com/a/9666741/1306679
        private void materiali_prestazioni_SelectionChanged(object sender, EventArgs e)
        {
            Utilities.NextCellFocusHandler((DataGridView)sender, ultimaCellaEditata);
        }
        private void materiali_prestazioni_modified(object sender, DataGridViewCellEventArgs e)
        {
            ultimaCellaEditata = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex]; // necessario per "spostamento" a cella successiva
            if(e.ColumnIndex == 0) // gestiamo  solamente l'inserimento del campo "Tipo" o del nome materiale, non le quantità/ore!
                controllaNomiNonCorretti((DataGridView)sender, ultimaCellaEditata);
            aggiornaTotali();
        }

        /// <summary>
        /// Controlla che l'ultimo inserimento di nome materiale o tipo contratto sia effettivmaente esistente su DB
        /// </summary>
        public void controllaNomiNonCorretti(DataGridView datagrid, DataGridViewCell cellaModificata)
        {
            string valoreInserito = ultimaCellaEditata.FormattedValue.ToString();
            bool valoreEsistente = true;

            // controllo che il nome di materiale (o di contratto) inserito dall'utente corrisponda a qualcosa. in caso contrario, glielo segnalo
            if (datagrid == datagrid_materiali)
                valoreEsistente = valoreInserito != string.Empty && materialiCacheati.Any(m => m.Descrizione == valoreInserito.ToString() || m.Abbreviazione == valoreInserito);
    
            if (datagrid == datagrid_prestazioni)
                valoreEsistente = valoreInserito != string.Empty && contrattiCacheati.Any(tipoContratto => tipoContratto.Nome == valoreInserito);

            datagrid.Rows[cellaModificata.RowIndex].HeaderCell.Style.BackColor = valoreEsistente ? datagrid.RowHeadersDefaultCellStyle.BackColor : Color.OrangeRed;
        }

        private void chk_stampato_CheckedChanged(object sender, EventArgs e)
        {
            lavoro.Segnato = chk_stampato.Checked;
        }
    }
}
