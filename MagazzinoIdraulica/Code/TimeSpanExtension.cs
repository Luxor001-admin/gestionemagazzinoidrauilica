﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagazzinoIdraulica.Code
{
    public static class TimeSpanExtension
    {
        public static string OreMinutiFormattate(this TimeSpan timespan)
        {
            return string.Format("{0:00}h:{1:00}m", (int)timespan.TotalHours, timespan.Minutes); // faccio un cast a int per rimuovere parti decimali di totalHours. 10 ore e mezza(630min) sono 10.5 TotalHours, che potrebbe essere mostrato come 11 nella cellvalue perché fà un arrotondamento!
        }
    }
}
