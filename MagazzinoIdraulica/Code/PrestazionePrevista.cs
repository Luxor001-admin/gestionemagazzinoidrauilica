﻿using MagazzinoIdraulicaDB.Models;

namespace MagazzinoIdraulica.Code
{
    public class PrestazionePrevista
    {
        public TipoContratto tipoContratto { get; set; }
        public long minuti { get; set; }

        public PrestazionePrevista(TipoContratto tipoContratto, long minuti)
        {
            this.tipoContratto = tipoContratto;
            this.minuti = minuti;
        }

        public decimal calcolaPaga()
        {
            return (tipoContratto.PagaOraria / 60) * this.minuti;
        }
    }
}
