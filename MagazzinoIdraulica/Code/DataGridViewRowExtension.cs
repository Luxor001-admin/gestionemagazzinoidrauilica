﻿using System.Globalization;
using System.Windows.Forms;

namespace MagazzinoIdraulica.Code
{
    public static class DataGridViewRowExtension
    {
        public static bool ValidateDecimalCell(this DataGridView grid, DataGridViewCellValidatingEventArgs e)
        {
            string valoreCella = grid.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString();
            bool riuscito = decimal.TryParse(valoreCella, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal valore);
            if(!riuscito || valore <= 0)
            {
                grid.CancelEdit();
                grid.EndEdit();
            }
            return riuscito;
        }

        public static double ValueToDouble(this DataGridViewCell cell)
        {
            bool riuscito = double.TryParse(cell.EditedFormattedValue.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out double valore);
            return riuscito ? valore : 0;
        }
        public static decimal ValueToDecimal(this DataGridViewCell cell)
        {
            bool riuscito = decimal.TryParse(cell.EditedFormattedValue.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal valore);
            return riuscito ? valore : 0;
        }

        public static bool ValidateIntCell(this DataGridView grid, DataGridViewCellValidatingEventArgs e, int minimumAcceptableValue)
        {
            string valoreCella = grid.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString();
            bool riuscito = int.TryParse(valoreCella, out int valore);
            if (!riuscito || valore < minimumAcceptableValue)
            {
                grid.CancelEdit();
                grid.EndEdit();
            }
            return riuscito;
        }
        public static int ValueToInt(this DataGridViewCell cell)
        {
            bool riuscito = int.TryParse(cell.EditedFormattedValue.ToString(), out int valore);
            return riuscito ? valore : 0;
        }
    }
}