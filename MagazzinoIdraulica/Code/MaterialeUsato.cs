﻿using MagazzinoIdraulicaDB.Models;

namespace MagazzinoIdraulica.Code
{
    public class MaterialeUsato
    {
        public Materiale materiale { get; set; }
        public decimal quantita { get; set; }

        public MaterialeUsato(Materiale materiale, decimal quantita)
        {
            this.materiale = materiale;
            this.quantita = quantita;
        }
    }
}
