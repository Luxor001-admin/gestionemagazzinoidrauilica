﻿namespace MagazzinoIdraulica
{
    partial class Mainform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainform));
            this.cnt_clientiCantieri = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cntx_aggiungiCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.immagini = new System.Windows.Forms.ImageList(this.components);
            this.cnt_cliente = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aggiungiCantiereToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rinominaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.calcolaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.datagrid_lavori = new System.Windows.Forms.DataGridView();
            this.Segnato_column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipologia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Referente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Indirizzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Totale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cnt_lavoro = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.stampaLavoriSelezionatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.spostaSelezionatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cancellaLavoriSelezionatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnl_clientiCantieri = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnl_treeview = new System.Windows.Forms.Panel();
            this.tree_clientiCantieri = new System.Windows.Forms.TreeView();
            this.pnl_barraRicerca = new System.Windows.Forms.Panel();
            this.btn_cercaCliente = new System.Windows.Forms.Button();
            this.txt_cercaCliente = new System.Windows.Forms.TextBox();
            this.cnt_cantiere = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tlstp_spostaLavoriSelezionati = new System.Windows.Forms.ToolStripMenuItem();
            this.tlstrp_separator_sposta = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ribbon = new System.Windows.Forms.Ribbon();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem2 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
            this.btn_reinizializza = new System.Windows.Forms.RibbonButton();
            this.ribbonButton8 = new System.Windows.Forms.RibbonButton();
            this.tab_home = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.btn_aggiungiCliente = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.btn_gestioneListino = new System.Windows.Forms.RibbonButton();
            this.btn_gestioneTipologie = new System.Windows.Forms.RibbonButton();
            this.btn_gestioneContratti = new System.Windows.Forms.RibbonButton();
            this.tab_cliente = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.tab_cantiere = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.btn_aggiungiLavoroTabCantiere = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton6 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton5 = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.ribbonButton9 = new System.Windows.Forms.RibbonButton();
            this.btn_stampaCantiere = new System.Windows.Forms.RibbonButton();
            this.btn_stampaSelezionati = new System.Windows.Forms.RibbonButton();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.cnt_clientiCantieri.SuspendLayout();
            this.cnt_cliente.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_lavori)).BeginInit();
            this.cnt_lavoro.SuspendLayout();
            this.pnl_clientiCantieri.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnl_treeview.SuspendLayout();
            this.pnl_barraRicerca.SuspendLayout();
            this.cnt_cantiere.SuspendLayout();
            this.SuspendLayout();
            // 
            // cnt_clientiCantieri
            // 
            this.cnt_clientiCantieri.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cnt_clientiCantieri.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cnt_clientiCantieri.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cntx_aggiungiCliente});
            this.cnt_clientiCantieri.Name = "cnt_clientiCantieri";
            this.cnt_clientiCantieri.Size = new System.Drawing.Size(190, 30);
            // 
            // cntx_aggiungiCliente
            // 
            this.cntx_aggiungiCliente.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cntx_aggiungiCliente.Image = global::MagazzinoIdraulica.Properties.Resources.icon_cliente;
            this.cntx_aggiungiCliente.Name = "cntx_aggiungiCliente";
            this.cntx_aggiungiCliente.Size = new System.Drawing.Size(189, 26);
            this.cntx_aggiungiCliente.Text = "Aggiungi cliente...";
            this.cntx_aggiungiCliente.Click += new System.EventHandler(this.AggiungiCliente);
            // 
            // immagini
            // 
            this.immagini.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.immagini.ImageSize = new System.Drawing.Size(16, 16);
            this.immagini.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cnt_cliente
            // 
            this.cnt_cliente.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cnt_cliente.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cnt_cliente.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiungiCantiereToolStripMenuItem1,
            this.rinominaToolStripMenuItem,
            this.eliminaToolStripMenuItem,
            this.toolStripSeparator1,
            this.calcolaToolStripMenuItem});
            this.cnt_cliente.Name = "cnt_cliente";
            this.cnt_cliente.Size = new System.Drawing.Size(199, 114);
            // 
            // aggiungiCantiereToolStripMenuItem1
            // 
            this.aggiungiCantiereToolStripMenuItem1.Image = global::MagazzinoIdraulica.Properties.Resources.icon_cantiere;
            this.aggiungiCantiereToolStripMenuItem1.Name = "aggiungiCantiereToolStripMenuItem1";
            this.aggiungiCantiereToolStripMenuItem1.Size = new System.Drawing.Size(198, 26);
            this.aggiungiCantiereToolStripMenuItem1.Text = "Aggiungi cantiere...";
            this.aggiungiCantiereToolStripMenuItem1.Click += new System.EventHandler(this.AggiungiCantiere);
            // 
            // rinominaToolStripMenuItem
            // 
            this.rinominaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.rinominaToolStripMenuItem.Name = "rinominaToolStripMenuItem";
            this.rinominaToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.rinominaToolStripMenuItem.Text = "Rinomina";
            this.rinominaToolStripMenuItem.Click += new System.EventHandler(this.RinominaCliente);
            // 
            // eliminaToolStripMenuItem
            // 
            this.eliminaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.eliminaToolStripMenuItem.Name = "eliminaToolStripMenuItem";
            this.eliminaToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.eliminaToolStripMenuItem.Text = "Elimina";
            this.eliminaToolStripMenuItem.Click += new System.EventHandler(this.EliminaCliente);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // calcolaToolStripMenuItem
            // 
            this.calcolaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.calcolaToolStripMenuItem.Name = "calcolaToolStripMenuItem";
            this.calcolaToolStripMenuItem.Size = new System.Drawing.Size(198, 26);
            this.calcolaToolStripMenuItem.Text = "Stampa...";
            this.calcolaToolStripMenuItem.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.pnl_clientiCantieri);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 120, 0, 0);
            this.panel1.Size = new System.Drawing.Size(1664, 629);
            this.panel1.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.datagrid_lavori);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.groupBox2.Location = new System.Drawing.Point(512, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1152, 509);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lavori";
            // 
            // datagrid_lavori
            // 
            this.datagrid_lavori.AllowUserToAddRows = false;
            this.datagrid_lavori.AllowUserToResizeRows = false;
            this.datagrid_lavori.ColumnHeadersHeight = 30;
            this.datagrid_lavori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.datagrid_lavori.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Segnato_column,
            this.Data,
            this.Tipologia,
            this.Note,
            this.Referente,
            this.Indirizzo,
            this.Totale});
            this.datagrid_lavori.ContextMenuStrip = this.cnt_lavoro;
            this.datagrid_lavori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagrid_lavori.EnableHeadersVisualStyles = false;
            this.datagrid_lavori.Location = new System.Drawing.Point(3, 19);
            this.datagrid_lavori.Margin = new System.Windows.Forms.Padding(2);
            this.datagrid_lavori.Name = "datagrid_lavori";
            this.datagrid_lavori.RowHeadersWidth = 40;
            this.datagrid_lavori.RowTemplate.Height = 24;
            this.datagrid_lavori.Size = new System.Drawing.Size(1146, 487);
            this.datagrid_lavori.TabIndex = 5;
            this.datagrid_lavori.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LavoroDoppioClicckato);
            this.datagrid_lavori.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.datagrid_lavori_CellMouseUp);
            this.datagrid_lavori.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagrid_lavori_CellValueChanged);
            this.datagrid_lavori.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.EventoDataGridEliminaLavoroDaCanc);
            this.datagrid_lavori.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datagrid_lavori_KeyDown);
            // 
            // Segnato_column
            // 
            this.Segnato_column.HeaderText = "";
            this.Segnato_column.MinimumWidth = 6;
            this.Segnato_column.Name = "Segnato_column";
            this.Segnato_column.Width = 40;
            // 
            // Data
            // 
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.Data.DefaultCellStyle = dataGridViewCellStyle1;
            this.Data.FillWeight = 10F;
            this.Data.HeaderText = "Data";
            this.Data.MinimumWidth = 6;
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.Width = 90;
            // 
            // Tipologia
            // 
            this.Tipologia.FillWeight = 15F;
            this.Tipologia.HeaderText = "Tipologia";
            this.Tipologia.MinimumWidth = 6;
            this.Tipologia.Name = "Tipologia";
            this.Tipologia.ReadOnly = true;
            this.Tipologia.Width = 180;
            // 
            // Note
            // 
            this.Note.HeaderText = "Note";
            this.Note.MinimumWidth = 6;
            this.Note.Name = "Note";
            this.Note.ReadOnly = true;
            this.Note.Width = 400;
            // 
            // Referente
            // 
            this.Referente.FillWeight = 20F;
            this.Referente.HeaderText = "Referente";
            this.Referente.MinimumWidth = 6;
            this.Referente.Name = "Referente";
            this.Referente.ReadOnly = true;
            this.Referente.Width = 200;
            // 
            // Indirizzo
            // 
            this.Indirizzo.FillWeight = 30F;
            this.Indirizzo.HeaderText = "Indirizzo";
            this.Indirizzo.MinimumWidth = 6;
            this.Indirizzo.Name = "Indirizzo";
            this.Indirizzo.ReadOnly = true;
            this.Indirizzo.Width = 200;
            // 
            // Totale
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.Totale.DefaultCellStyle = dataGridViewCellStyle2;
            this.Totale.FillWeight = 15F;
            this.Totale.HeaderText = "Totale";
            this.Totale.MinimumWidth = 6;
            this.Totale.Name = "Totale";
            this.Totale.ReadOnly = true;
            this.Totale.Width = 120;
            // 
            // cnt_lavoro
            // 
            this.cnt_lavoro.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cnt_lavoro.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cnt_lavoro.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stampaLavoriSelezionatiToolStripMenuItem,
            this.toolStripSeparator3,
            this.toolStripMenuItem1,
            this.toolStripMenuItem5,
            this.toolStripSeparator4,
            this.spostaSelezionatiToolStripMenuItem,
            this.toolStripSeparator5,
            this.cancellaLavoriSelezionatiToolStripMenuItem});
            this.cnt_lavoro.Name = "cnt_lavoro";
            this.cnt_lavoro.Size = new System.Drawing.Size(232, 152);
            this.cnt_lavoro.Opening += new System.ComponentModel.CancelEventHandler(this.cnt_lavoro_Opening);
            // 
            // stampaLavoriSelezionatiToolStripMenuItem
            // 
            this.stampaLavoriSelezionatiToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.stampaLavoriSelezionatiToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.stampaLavoriSelezionatiToolStripMenuItem.Name = "stampaLavoriSelezionatiToolStripMenuItem";
            this.stampaLavoriSelezionatiToolStripMenuItem.Size = new System.Drawing.Size(231, 26);
            this.stampaLavoriSelezionatiToolStripMenuItem.Text = "Stampa";
            this.stampaLavoriSelezionatiToolStripMenuItem.Click += new System.EventHandler(this.StampaLavoriSelezionati);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(228, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(231, 26);
            this.toolStripMenuItem1.Text = "Imposta stato \"Segnato\"";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.ImpostaSegnati);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(231, 26);
            this.toolStripMenuItem5.Text = "Rimuovi stato \"Segnato\"";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.RimuoviSegnati);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(228, 6);
            // 
            // spostaSelezionatiToolStripMenuItem
            // 
            this.spostaSelezionatiToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.taglia;
            this.spostaSelezionatiToolStripMenuItem.Name = "spostaSelezionatiToolStripMenuItem";
            this.spostaSelezionatiToolStripMenuItem.Size = new System.Drawing.Size(231, 26);
            this.spostaSelezionatiToolStripMenuItem.Text = "Taglia";
            this.spostaSelezionatiToolStripMenuItem.Click += new System.EventHandler(this.SelezionaLavoriDaSpostare_click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(228, 6);
            // 
            // cancellaLavoriSelezionatiToolStripMenuItem
            // 
            this.cancellaLavoriSelezionatiToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.cancellaLavoriSelezionatiToolStripMenuItem.Name = "cancellaLavoriSelezionatiToolStripMenuItem";
            this.cancellaLavoriSelezionatiToolStripMenuItem.Size = new System.Drawing.Size(231, 26);
            this.cancellaLavoriSelezionatiToolStripMenuItem.Text = "Elimina";
            this.cancellaLavoriSelezionatiToolStripMenuItem.Click += new System.EventHandler(this.cancellaLavoriSelezionatiToolStripMenuItem_Click);
            // 
            // pnl_clientiCantieri
            // 
            this.pnl_clientiCantieri.Controls.Add(this.panel2);
            this.pnl_clientiCantieri.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_clientiCantieri.Location = new System.Drawing.Point(0, 120);
            this.pnl_clientiCantieri.Name = "pnl_clientiCantieri";
            this.pnl_clientiCantieri.Size = new System.Drawing.Size(512, 509);
            this.pnl_clientiCantieri.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(512, 509);
            this.panel2.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnl_treeview);
            this.groupBox1.Controls.Add(this.pnl_barraRicerca);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(512, 509);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Clienti/cantieri";
            // 
            // pnl_treeview
            // 
            this.pnl_treeview.Controls.Add(this.tree_clientiCantieri);
            this.pnl_treeview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_treeview.Location = new System.Drawing.Point(2, 43);
            this.pnl_treeview.Name = "pnl_treeview";
            this.pnl_treeview.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.pnl_treeview.Size = new System.Drawing.Size(508, 464);
            this.pnl_treeview.TabIndex = 8;
            // 
            // tree_clientiCantieri
            // 
            this.tree_clientiCantieri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tree_clientiCantieri.ContextMenuStrip = this.cnt_clientiCantieri;
            this.tree_clientiCantieri.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tree_clientiCantieri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree_clientiCantieri.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tree_clientiCantieri.FullRowSelect = true;
            this.tree_clientiCantieri.HideSelection = false;
            this.tree_clientiCantieri.ImageIndex = 0;
            this.tree_clientiCantieri.ImageList = this.immagini;
            this.tree_clientiCantieri.Indent = 18;
            this.tree_clientiCantieri.ItemHeight = 25;
            this.tree_clientiCantieri.Location = new System.Drawing.Point(0, 5);
            this.tree_clientiCantieri.Margin = new System.Windows.Forms.Padding(2);
            this.tree_clientiCantieri.Name = "tree_clientiCantieri";
            this.tree_clientiCantieri.SelectedImageIndex = 0;
            this.tree_clientiCantieri.Size = new System.Drawing.Size(508, 459);
            this.tree_clientiCantieri.TabIndex = 1;
            this.tree_clientiCantieri.TabStop = false;
            this.tree_clientiCantieri.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.NodoSelezionato);
            // 
            // pnl_barraRicerca
            // 
            this.pnl_barraRicerca.Controls.Add(this.btn_cercaCliente);
            this.pnl_barraRicerca.Controls.Add(this.txt_cercaCliente);
            this.pnl_barraRicerca.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_barraRicerca.Location = new System.Drawing.Point(2, 18);
            this.pnl_barraRicerca.Name = "pnl_barraRicerca";
            this.pnl_barraRicerca.Size = new System.Drawing.Size(508, 25);
            this.pnl_barraRicerca.TabIndex = 7;
            // 
            // btn_cercaCliente
            // 
            this.btn_cercaCliente.BackColor = System.Drawing.Color.Transparent;
            this.btn_cercaCliente.BackgroundImage = global::MagazzinoIdraulica.Properties.Resources.cerca;
            this.btn_cercaCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cercaCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_cercaCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_cercaCliente.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_cercaCliente.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_cercaCliente.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btn_cercaCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cercaCliente.ForeColor = System.Drawing.Color.Transparent;
            this.btn_cercaCliente.Location = new System.Drawing.Point(483, 0);
            this.btn_cercaCliente.Name = "btn_cercaCliente";
            this.btn_cercaCliente.Size = new System.Drawing.Size(25, 25);
            this.btn_cercaCliente.TabIndex = 1;
            this.btn_cercaCliente.UseVisualStyleBackColor = false;
            this.btn_cercaCliente.Click += new System.EventHandler(this.btn_cercaCliente_Click);
            // 
            // txt_cercaCliente
            // 
            this.txt_cercaCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txt_cercaCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt_cercaCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_cercaCliente.Dock = System.Windows.Forms.DockStyle.Left;
            this.txt_cercaCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txt_cercaCliente.Location = new System.Drawing.Point(0, 0);
            this.txt_cercaCliente.Name = "txt_cercaCliente";
            this.txt_cercaCliente.Size = new System.Drawing.Size(483, 26);
            this.txt_cercaCliente.TabIndex = 0;
            this.txt_cercaCliente.Enter += new System.EventHandler(this.txt_cercaCliente_focus);
            this.txt_cercaCliente.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_cercaCliente_KeyUp);
            // 
            // cnt_cantiere
            // 
            this.cnt_cantiere.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cnt_cantiere.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cnt_cantiere.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripSeparator2,
            this.tlstp_spostaLavoriSelezionati,
            this.tlstrp_separator_sposta,
            this.toolStripMenuItem4});
            this.cnt_cantiere.Name = "cnt_cantiere";
            this.cnt_cantiere.Size = new System.Drawing.Size(198, 120);
            this.cnt_cantiere.Opening += new System.ComponentModel.CancelEventHandler(this.cnt_cantiere_opening);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(197, 26);
            this.toolStripMenuItem2.Text = "Rinomina";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.RinominaCantiere);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Image = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(197, 26);
            this.toolStripMenuItem3.Text = "Elimina";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.EliminaCantiere);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(194, 6);
            // 
            // tlstp_spostaLavoriSelezionati
            // 
            this.tlstp_spostaLavoriSelezionati.Image = global::MagazzinoIdraulica.Properties.Resources.inserisci;
            this.tlstp_spostaLavoriSelezionati.Name = "tlstp_spostaLavoriSelezionati";
            this.tlstp_spostaLavoriSelezionati.Size = new System.Drawing.Size(197, 26);
            this.tlstp_spostaLavoriSelezionati.Text = "Sposta N lavori qui";
            this.tlstp_spostaLavoriSelezionati.Click += new System.EventHandler(this.SpostaLavoriSelezionati_click);
            // 
            // tlstrp_separator_sposta
            // 
            this.tlstrp_separator_sposta.Name = "tlstrp_separator_sposta";
            this.tlstrp_separator_sposta.Size = new System.Drawing.Size(194, 6);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Image = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(197, 26);
            this.toolStripMenuItem4.Text = "Stampa...";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.StampaCantiere);
            // 
            // ribbon
            // 
            this.ribbon.CaptionBarVisible = false;
            this.ribbon.Cursor = System.Windows.Forms.Cursors.Default;
            this.ribbon.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Minimized = false;
            this.ribbon.Name = "ribbon";
            // 
            // 
            // 
            this.ribbon.OrbDropDown.BorderRoundness = 8;
            this.ribbon.OrbDropDown.ContentButtonsMinWidth = 5;
            this.ribbon.OrbDropDown.ContentRecentItemsMinWidth = 5;
            this.ribbon.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem1);
            this.ribbon.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem2);
            this.ribbon.OrbDropDown.Name = "";
            this.ribbon.OrbDropDown.Size = new System.Drawing.Size(0, 160);
            this.ribbon.OrbDropDown.TabIndex = 0;
            this.ribbon.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010_Extended;
            this.ribbon.OrbText = "File";
            this.ribbon.OrbVisible = false;
            // 
            // 
            // 
            this.ribbon.QuickAccessToolbar.Visible = false;
            this.ribbon.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 10F);
            this.ribbon.Size = new System.Drawing.Size(1664, 118);
            this.ribbon.TabIndex = 1;
            this.ribbon.Tabs.Add(this.ribbonTab1);
            this.ribbon.Tabs.Add(this.tab_home);
            this.ribbon.Tabs.Add(this.tab_cliente);
            this.ribbon.Tabs.Add(this.tab_cantiere);
            this.ribbon.TabSpacing = 3;
            this.ribbon.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.Image")));
            this.ribbonOrbMenuItem1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.LargeImage")));
            this.ribbonOrbMenuItem1.Name = "ribbonOrbMenuItem1";
            this.ribbonOrbMenuItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem1.SmallImage")));
            // 
            // ribbonOrbMenuItem2
            // 
            this.ribbonOrbMenuItem2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.Image")));
            this.ribbonOrbMenuItem2.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.LargeImage")));
            this.ribbonOrbMenuItem2.Name = "ribbonOrbMenuItem2";
            this.ribbonOrbMenuItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbMenuItem2.SmallImage")));
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Panels.Add(this.ribbonPanel8);
            this.ribbonTab1.Text = "File";
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.Enabled = false;
            this.ribbonPanel8.Items.Add(this.btn_reinizializza);
            this.ribbonPanel8.Items.Add(this.ribbonButton8);
            this.ribbonPanel8.Name = "ribbonPanel8";
            this.ribbonPanel8.Text = "";
            // 
            // btn_reinizializza
            // 
            this.btn_reinizializza.Image = global::MagazzinoIdraulica.Properties.Resources.pulisci;
            this.btn_reinizializza.LargeImage = global::MagazzinoIdraulica.Properties.Resources.pulisci;
            this.btn_reinizializza.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_reinizializza.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_reinizializza.Name = "btn_reinizializza";
            this.btn_reinizializza.SmallImage = global::MagazzinoIdraulica.Properties.Resources.pulisci;
            this.btn_reinizializza.Text = "Reinizializza";
            this.btn_reinizializza.Visible = false;
            this.btn_reinizializza.Click += new System.EventHandler(this.Reinizializza);
            // 
            // ribbonButton8
            // 
            this.ribbonButton8.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton8.Image")));
            this.ribbonButton8.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton8.LargeImage")));
            this.ribbonButton8.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton8.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton8.Name = "ribbonButton8";
            this.ribbonButton8.SmallImage = global::MagazzinoIdraulica.Properties.Resources.esci;
            this.ribbonButton8.Text = "Esci\t";
            this.ribbonButton8.Click += new System.EventHandler(this.Esci);
            // 
            // tab_home
            // 
            this.tab_home.Name = "tab_home";
            this.tab_home.Panels.Add(this.ribbonPanel3);
            this.tab_home.Panels.Add(this.ribbonPanel1);
            this.tab_home.Text = "Home";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ButtonMoreVisible = false;
            this.ribbonPanel3.Items.Add(this.btn_aggiungiCliente);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Text = "";
            // 
            // btn_aggiungiCliente
            // 
            this.btn_aggiungiCliente.Image = global::MagazzinoIdraulica.Properties.Resources.icon_cliente;
            this.btn_aggiungiCliente.LargeImage = global::MagazzinoIdraulica.Properties.Resources.icon_cliente;
            this.btn_aggiungiCliente.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btn_aggiungiCliente.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btn_aggiungiCliente.Name = "btn_aggiungiCliente";
            this.btn_aggiungiCliente.SmallImage = global::MagazzinoIdraulica.Properties.Resources.icon_cliente;
            this.btn_aggiungiCliente.Text = "Aggiungi cliente";
            this.btn_aggiungiCliente.Click += new System.EventHandler(this.AggiungiCliente);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ButtonMoreEnabled = false;
            this.ribbonPanel1.ButtonMoreVisible = false;
            this.ribbonPanel1.Items.Add(this.btn_gestioneListino);
            this.ribbonPanel1.Items.Add(this.btn_gestioneTipologie);
            this.ribbonPanel1.Items.Add(this.btn_gestioneContratti);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Text = "Gestione";
            // 
            // btn_gestioneListino
            // 
            this.btn_gestioneListino.Image = global::MagazzinoIdraulica.Properties.Resources.listino_icon;
            this.btn_gestioneListino.LargeImage = global::MagazzinoIdraulica.Properties.Resources.listino_icon;
            this.btn_gestioneListino.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_gestioneListino.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_gestioneListino.Name = "btn_gestioneListino";
            this.btn_gestioneListino.SmallImage = global::MagazzinoIdraulica.Properties.Resources.listino_icon;
            this.btn_gestioneListino.Text = "Listino";
            this.btn_gestioneListino.Click += new System.EventHandler(this.GestioneCatalogoClicked);
            // 
            // btn_gestioneTipologie
            // 
            this.btn_gestioneTipologie.Image = global::MagazzinoIdraulica.Properties.Resources.tipologie;
            this.btn_gestioneTipologie.LargeImage = global::MagazzinoIdraulica.Properties.Resources.tipologie;
            this.btn_gestioneTipologie.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_gestioneTipologie.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_gestioneTipologie.Name = "btn_gestioneTipologie";
            this.btn_gestioneTipologie.SmallImage = global::MagazzinoIdraulica.Properties.Resources.tipologie;
            this.btn_gestioneTipologie.Text = "Tipologie";
            this.btn_gestioneTipologie.Click += new System.EventHandler(this.GestioneTipologieClicked);
            // 
            // btn_gestioneContratti
            // 
            this.btn_gestioneContratti.Image = global::MagazzinoIdraulica.Properties.Resources.worker;
            this.btn_gestioneContratti.LargeImage = global::MagazzinoIdraulica.Properties.Resources.worker;
            this.btn_gestioneContratti.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_gestioneContratti.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.btn_gestioneContratti.Name = "btn_gestioneContratti";
            this.btn_gestioneContratti.SmallImage = global::MagazzinoIdraulica.Properties.Resources.worker;
            this.btn_gestioneContratti.Text = "Contratti";
            this.btn_gestioneContratti.Click += new System.EventHandler(this.GestioneContrattiClicked);
            // 
            // tab_cliente
            // 
            this.tab_cliente.Enabled = false;
            this.tab_cliente.Name = "tab_cliente";
            this.tab_cliente.Panels.Add(this.ribbonPanel4);
            this.tab_cliente.Panels.Add(this.ribbonPanel5);
            this.tab_cliente.Text = "Cliente";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ButtonMoreVisible = false;
            this.ribbonPanel4.Enabled = false;
            this.ribbonPanel4.Items.Add(this.ribbonButton1);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Text = "";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Enabled = false;
            this.ribbonButton1.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.Image")));
            this.ribbonButton1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.LargeImage")));
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "Aggiungi cantiere";
            this.ribbonButton1.Click += new System.EventHandler(this.AggiungiCantiere);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ButtonMoreVisible = false;
            this.ribbonPanel5.Enabled = false;
            this.ribbonPanel5.Items.Add(this.ribbonButton3);
            this.ribbonPanel5.Items.Add(this.ribbonButton2);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Text = "";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Enabled = false;
            this.ribbonButton3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.Image")));
            this.ribbonButton3.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.LargeImage")));
            this.ribbonButton3.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton3.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton3.Name = "ribbonButton3";
            this.ribbonButton3.SmallImage = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.ribbonButton3.Text = "Rinomina";
            this.ribbonButton3.Click += new System.EventHandler(this.RinominaCliente);
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.Enabled = false;
            this.ribbonButton2.Image = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.ribbonButton2.LargeImage = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.ribbonButton2.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton2.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton2.Name = "ribbonButton2";
            this.ribbonButton2.SmallImage = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.ribbonButton2.Text = "Elimina";
            this.ribbonButton2.Click += new System.EventHandler(this.EliminaCliente);
            // 
            // tab_cantiere
            // 
            this.tab_cantiere.Enabled = false;
            this.tab_cantiere.Name = "tab_cantiere";
            this.tab_cantiere.Panels.Add(this.ribbonPanel2);
            this.tab_cantiere.Panels.Add(this.ribbonPanel6);
            this.tab_cantiere.Panels.Add(this.ribbonPanel9);
            this.tab_cantiere.Text = "Cantiere";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ButtonMoreVisible = false;
            this.ribbonPanel2.Enabled = false;
            this.ribbonPanel2.Items.Add(this.btn_aggiungiLavoroTabCantiere);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Text = "";
            // 
            // btn_aggiungiLavoroTabCantiere
            // 
            this.btn_aggiungiLavoroTabCantiere.Enabled = false;
            this.btn_aggiungiLavoroTabCantiere.Image = global::MagazzinoIdraulica.Properties.Resources.aggiungi;
            this.btn_aggiungiLavoroTabCantiere.LargeImage = global::MagazzinoIdraulica.Properties.Resources.aggiungi;
            this.btn_aggiungiLavoroTabCantiere.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btn_aggiungiLavoroTabCantiere.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btn_aggiungiLavoroTabCantiere.Name = "btn_aggiungiLavoroTabCantiere";
            this.btn_aggiungiLavoroTabCantiere.SmallImage = global::MagazzinoIdraulica.Properties.Resources.aggiungi;
            this.btn_aggiungiLavoroTabCantiere.Text = "Aggiungi lavoro";
            this.btn_aggiungiLavoroTabCantiere.Click += new System.EventHandler(this.AggiungiLavoro);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ButtonMoreVisible = false;
            this.ribbonPanel6.Enabled = false;
            this.ribbonPanel6.Items.Add(this.ribbonButton4);
            this.ribbonPanel6.Items.Add(this.ribbonButton6);
            this.ribbonPanel6.Items.Add(this.ribbonButton5);
            this.ribbonPanel6.Name = "ribbonPanel6";
            this.ribbonPanel6.Text = "";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.Enabled = false;
            this.ribbonButton4.Image = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.ribbonButton4.LargeImage = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.ribbonButton4.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton4.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton4.Name = "ribbonButton4";
            this.ribbonButton4.SmallImage = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.ribbonButton4.Text = "Rinomina";
            this.ribbonButton4.Click += new System.EventHandler(this.RinominaCantiere);
            // 
            // ribbonButton6
            // 
            this.ribbonButton6.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.Image")));
            this.ribbonButton6.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.LargeImage")));
            this.ribbonButton6.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton6.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton6.Name = "ribbonButton6";
            this.ribbonButton6.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.SmallImage")));
            this.ribbonButton6.Text = "Sposta";
            this.ribbonButton6.Click += new System.EventHandler(this.SpostaCantiere);
            // 
            // ribbonButton5
            // 
            this.ribbonButton5.Enabled = false;
            this.ribbonButton5.Image = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.ribbonButton5.LargeImage = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.ribbonButton5.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton5.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.ribbonButton5.Name = "ribbonButton5";
            this.ribbonButton5.SmallImage = global::MagazzinoIdraulica.Properties.Resources.cancella;
            this.ribbonButton5.Text = "Cancella";
            this.ribbonButton5.Click += new System.EventHandler(this.EliminaCantiere);
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ButtonMoreVisible = false;
            this.ribbonPanel9.Enabled = false;
            this.ribbonPanel9.Items.Add(this.ribbonButton9);
            this.ribbonPanel9.Name = "ribbonPanel9";
            this.ribbonPanel9.Text = "";
            // 
            // ribbonButton9
            // 
            this.ribbonButton9.DropDownItems.Add(this.btn_stampaCantiere);
            this.ribbonButton9.DropDownItems.Add(this.btn_stampaSelezionati);
            this.ribbonButton9.Enabled = false;
            this.ribbonButton9.Image = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.ribbonButton9.LargeImage = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.ribbonButton9.Name = "ribbonButton9";
            this.ribbonButton9.SmallImage = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.ribbonButton9.Style = System.Windows.Forms.RibbonButtonStyle.SplitDropDown;
            this.ribbonButton9.Text = "Stampa report";
            this.ribbonButton9.DropDownShowing += new System.EventHandler(this.StampaDropDownShowing);
            // 
            // btn_stampaCantiere
            // 
            this.btn_stampaCantiere.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btn_stampaCantiere.Image = ((System.Drawing.Image)(resources.GetObject("btn_stampaCantiere.Image")));
            this.btn_stampaCantiere.LargeImage = ((System.Drawing.Image)(resources.GetObject("btn_stampaCantiere.LargeImage")));
            this.btn_stampaCantiere.Name = "btn_stampaCantiere";
            this.btn_stampaCantiere.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_stampaCantiere.SmallImage")));
            this.btn_stampaCantiere.Text = "Stampa cantiere...";
            this.btn_stampaCantiere.Click += new System.EventHandler(this.StampaCantiere);
            // 
            // btn_stampaSelezionati
            // 
            this.btn_stampaSelezionati.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btn_stampaSelezionati.Image = ((System.Drawing.Image)(resources.GetObject("btn_stampaSelezionati.Image")));
            this.btn_stampaSelezionati.LargeImage = ((System.Drawing.Image)(resources.GetObject("btn_stampaSelezionati.LargeImage")));
            this.btn_stampaSelezionati.Name = "btn_stampaSelezionati";
            this.btn_stampaSelezionati.SmallImage = ((System.Drawing.Image)(resources.GetObject("btn_stampaSelezionati.SmallImage")));
            this.btn_stampaSelezionati.Text = "Stampa lavori selezionati...";
            this.btn_stampaSelezionati.Click += new System.EventHandler(this.StampaLavoriSelezionati);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::MagazzinoIdraulica.Properties.Resources.modifica;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.MinimumWidth = 6;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Width = 25;
            // 
            // Mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1664, 629);
            this.Controls.Add(this.ribbon);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Mainform";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Il plastichino";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormInChiusura);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.cnt_clientiCantieri.ResumeLayout(false);
            this.cnt_cliente.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_lavori)).EndInit();
            this.cnt_lavoro.ResumeLayout(false);
            this.pnl_clientiCantieri.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.pnl_treeview.ResumeLayout(false);
            this.pnl_barraRicerca.ResumeLayout(false);
            this.pnl_barraRicerca.PerformLayout();
            this.cnt_cantiere.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip cnt_clientiCantieri;
        private System.Windows.Forms.ToolStripMenuItem cntx_aggiungiCliente;
        private System.Windows.Forms.ImageList immagini;
        private System.Windows.Forms.ContextMenuStrip cnt_cliente;
        private System.Windows.Forms.ToolStripMenuItem aggiungiCantiereToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rinominaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem calcolaToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnl_clientiCantieri;
        private System.Windows.Forms.ContextMenuStrip cnt_cantiere;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ContextMenuStrip cnt_lavoro;
        private System.Windows.Forms.ToolStripMenuItem stampaLavoriSelezionatiToolStripMenuItem;
        private System.Windows.Forms.Ribbon ribbon;
        private System.Windows.Forms.RibbonTab tab_home;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonButton btn_gestioneListino;
        private System.Windows.Forms.RibbonButton btn_gestioneTipologie;
        private System.Windows.Forms.RibbonButton btn_gestioneContratti;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView datagrid_lavori;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonButton btn_aggiungiCliente;
        private System.Windows.Forms.RibbonTab tab_cliente;
        private System.Windows.Forms.RibbonTab tab_cantiere;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonButton ribbonButton2;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonButton btn_aggiungiLavoroTabCantiere;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonButton ribbonButton4;
        private System.Windows.Forms.RibbonButton ribbonButton5;
        private System.Windows.Forms.RibbonPanel ribbonPanel7;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem2;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel8;
        private System.Windows.Forms.RibbonButton btn_reinizializza;
        private System.Windows.Forms.RibbonButton ribbonButton8;
        private System.Windows.Forms.RibbonPanel ribbonPanel9;
        private System.Windows.Forms.RibbonButton ribbonButton9;
        private System.Windows.Forms.RibbonButton btn_stampaCantiere;
        private System.Windows.Forms.RibbonButton btn_stampaSelezionati;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cancellaLavoriSelezionatiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.RibbonButton ribbonButton6;
        private System.Windows.Forms.ToolStripMenuItem spostaSelezionatiToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnl_barraRicerca;
        private System.Windows.Forms.TextBox txt_cercaCliente;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView tree_clientiCantieri;
        private System.Windows.Forms.Panel pnl_treeview;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Segnato_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipologia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Note;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Indirizzo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Totale;
        private System.Windows.Forms.Button btn_cercaCliente;
        private System.Windows.Forms.ToolStripMenuItem tlstp_spostaLavoriSelezionati;
        private System.Windows.Forms.ToolStripSeparator tlstrp_separator_sposta;
    }
}

