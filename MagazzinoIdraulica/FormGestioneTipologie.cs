﻿using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormGestioneTipologie : Form
    {
        dbContext dbContext;
        List<TipologiaLavoro> tipologieEliminate = new List<TipologiaLavoro>();
        public FormGestioneTipologie(dbContext dbContext)
        {
            this.dbContext = dbContext;
            InitializeComponent();
        }

        private void Salva(object sender, EventArgs e)
        {
            datagrid_tipologie.Rows.Cast<DataGridViewRow>()
                .Where(row => !row.IsNewRow)
                .Where(row => !string.IsNullOrEmpty(row.Cells["Tipologia"].EditedFormattedValue.ToString())) // ignoriamo tutte le righe con campo principale vuoto
                .ToList()
                .ForEach(rowTipologia =>
                {
                    TipologiaLavoro tipologia = rowTipologia.Tag as TipologiaLavoro ?? new TipologiaLavoro();
                    tipologia.Descrizione = rowTipologia.Cells["Tipologia"].EditedFormattedValue.ToString();
                    if (rowTipologia.Tag != null)
                        dbContext.Entry(tipologia).CurrentValues.SetValues(tipologia);
                    else
                        dbContext.TipologiaLavoro.Add(tipologia);
                });
            dbContext.TipologiaLavoro.RemoveRange(this.tipologieEliminate);
            dbContext.SaveChanges();
            this.Close();
        }

        private void GestioneTipologie_Load(object sender, EventArgs e)
        {
            this.PopolaDataGrid(dbContext.TipologiaLavoro);
        }

        private void PopolaDataGrid(IEnumerable<TipologiaLavoro> tipologie)
        {
            var righe = tipologie.Select(tipologia =>
            {
                DataGridViewRow row = (DataGridViewRow)datagrid_tipologie.RowTemplate.Clone();
                row.CreateCells(datagrid_tipologie);
                row.Cells[0].Value = tipologia.Descrizione;
                row.Tag = tipologia;
                return row;
            }).ToArray();
            datagrid_tipologie.Rows.AddRange(righe);
        }

        private void TipologiaEliminata(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Tag != null)
            {
                TipologiaLavoro tipologia = e.Row.Tag as TipologiaLavoro;
                if (tipologia.Lavoro.Count >= 1)
                {
                    MessageBox.Show("Non è possibile eliminare questa tipologia in quanto già in uso.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    return;
                }
                this.tipologieEliminate.Add(e.Row.Tag as TipologiaLavoro);
            }
        }

        private void Esci(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConvalidaCella(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (this.datagrid_tipologie.Columns[e.ColumnIndex].Name == "Tipologia")
            {
                string valoreCella = this.datagrid_tipologie.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString();
                if (valoreCella == string.Empty)
                    return;
                bool valoreGiaPresente = this.datagrid_tipologie.Rows.Cast<DataGridViewRow>().Any(row =>
                {
                    // se la riga non è quella corrente e c'è già qualche riga con la stesso nome di tipologia....
                    return row.Index != e.RowIndex && (valoreCella.ToLower() == row.Cells["Tipologia"].EditedFormattedValue.ToString().ToLower());
                });
                if (valoreGiaPresente)
                {
                    MessageBox.Show("Non è possibile inserire più tipologie con lo stesso nome", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    datagrid_tipologie.CancelEdit();
                    datagrid_tipologie.EndEdit();
                }
            }
        }

        private DataGridViewCell ultimaCellaEditata;
        private void datagrid_tipologie_modified(object sender, DataGridViewCellEventArgs e)
        {
            ultimaCellaEditata = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex]; // necessario per "spostamento" a cella successiva
            controllaNomiNonCorretti((DataGridView)sender, ultimaCellaEditata);
        }

        public void controllaNomiNonCorretti(DataGridView datagrid, DataGridViewCell cellaModificata)
        {
            bool valorePresente = cellaModificata.OwningRow.Cells[0].FormattedValue.ToString() != "";
            datagrid.Rows[cellaModificata.RowIndex].HeaderCell.Style.BackColor = valorePresente ? datagrid.RowHeadersDefaultCellStyle.BackColor : Color.OrangeRed;
        }
    }
}
