﻿namespace MagazzinoIdraulica
{
    partial class FormSpostaCantiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_annulla = new System.Windows.Forms.Button();
            this.btn_salva = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_nomeCliente = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_annulla
            // 
            this.btn_annulla.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_annulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.btn_annulla.Location = new System.Drawing.Point(218, 94);
            this.btn_annulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_annulla.Name = "btn_annulla";
            this.btn_annulla.Size = new System.Drawing.Size(103, 34);
            this.btn_annulla.TabIndex = 2;
            this.btn_annulla.Text = "Annulla";
            this.btn_annulla.UseVisualStyleBackColor = true;
            this.btn_annulla.Click += new System.EventHandler(this.Annulla);
            // 
            // btn_salva
            // 
            this.btn_salva.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.btn_salva.Location = new System.Drawing.Point(109, 94);
            this.btn_salva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_salva.Name = "btn_salva";
            this.btn_salva.Size = new System.Drawing.Size(103, 34);
            this.btn_salva.TabIndex = 1;
            this.btn_salva.Text = "Sposta";
            this.btn_salva.UseVisualStyleBackColor = true;
            this.btn_salva.Click += new System.EventHandler(this.Salva);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Cliente";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmb_nomeCliente
            // 
            this.cmb_nomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_nomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cmb_nomeCliente.FormattingEnabled = true;
            this.cmb_nomeCliente.Location = new System.Drawing.Point(15, 39);
            this.cmb_nomeCliente.Name = "cmb_nomeCliente";
            this.cmb_nomeCliente.Size = new System.Drawing.Size(309, 24);
            this.cmb_nomeCliente.TabIndex = 0;
            this.cmb_nomeCliente.TextChanged += new System.EventHandler(this.cmb_nomeCliente_textChanged);
            // 
            // FormSpostaCantiere
            // 
            this.AcceptButton = this.btn_salva;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_annulla;
            this.ClientSize = new System.Drawing.Size(337, 142);
            this.Controls.Add(this.cmb_nomeCliente);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_annulla);
            this.Controls.Add(this.btn_salva);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormSpostaCantiere";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sposta Cantiere";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormSpostaCantiere_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_annulla;
        private System.Windows.Forms.Button btn_salva;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_nomeCliente;
    }
}