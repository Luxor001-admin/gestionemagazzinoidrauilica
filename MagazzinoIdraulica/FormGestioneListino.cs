﻿using MagazzinoIdraulica.Code;
using MagazzinoIdraulicaDB.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormGestioneListino : Form
    {
        dbContext dbContext;
        List<Materiale> materialiDaEliminare = new List<Materiale>();
        bool soloLettura;
        public FormGestioneListino(dbContext dbContext, bool soloLettura = false)
        {
            this.dbContext = dbContext;
            this.soloLettura = soloLettura;
            InitializeComponent();
            this.datagrid_catalogo.Columns["Prezzo"].DefaultCellStyle.Format = "c";
            this.datagrid_catalogo.Columns["Prezzo"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("it-it");
            this.datagrid_catalogo.Columns["Prezzo"].ValueType = Type.GetType("System.Decimal");

            // Gestione modalità sola lettura: rendo immodificabile il listino e cambio il testo del form in "visualizzazione listino"

            if(soloLettura)
            {
                datagrid_catalogo.ReadOnly = true;
                salvaToolStripMenuItem.Visible = false;
                this.Text = "Visualizzazione Listino";
            }
        }

        private void Salva(object sender, EventArgs e)
        {
            datagrid_catalogo.Rows.Cast<DataGridViewRow>()
                .Where(row => !row.IsNewRow)
                // ignoriamo tutte le righe con campo principale vuoto
                .Where(row => !string.IsNullOrEmpty(row.Cells["Descrizione"].EditedFormattedValue.ToString()) || !string.IsNullOrEmpty(row.Cells["Abbreviazione"].EditedFormattedValue.ToString()))
                .ToList()
                .ForEach(rowMateriale =>
                {
                    Materiale materiale = (Materiale)rowMateriale.Tag ?? new Materiale();
                    materiale.Descrizione = rowMateriale.Cells["Descrizione"].EditedFormattedValue.ToString();
                    materiale.Abbreviazione = rowMateriale.Cells["Abbreviazione"].EditedFormattedValue.ToString();
                    materiale.Prezzo = rowMateriale.Cells["Prezzo"].ValueToDecimal();
                    if (rowMateriale.Tag != null)
                        dbContext.Entry(materiale).CurrentValues.SetValues(materiale);
                    else                    
                        dbContext.Materiale.Add(materiale);
                });

            dbContext.Materiale.RemoveRange(this.materialiDaEliminare);
            dbContext.SaveChanges();
            this.Close();
        }

        private void GestioneCatalogo_Load(object sender, EventArgs e)
        {
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            source.AddRange(dbContext.Materiale.Select(materiale => materiale.Descrizione).Distinct().ToArray());
            txt_ricerca.AutoCompleteCustomSource = source;
            this.PopolaDataGrid(dbContext.Materiale);
            this.AggiornaLabelCount();
        }

        private void TestoRicercaCambiato(object sender, EventArgs e)
        {
            string testoRicerca = (sender as TextBox).Text;
            this.PopolaDataGrid(dbContext.Materiale.Where(materiale => materiale.Descrizione.ToLower().Contains(testoRicerca.ToLower())));
            this.AggiornaLabelCount();
        }

        private void PopolaDataGrid(IEnumerable<Materiale> materiali)
        {
            this.datagrid_catalogo.Rows.Clear();
            var righe = materiali.Select(materiale =>
            {
                DataGridViewRow row = (DataGridViewRow)datagrid_catalogo.RowTemplate.Clone();
                row.CreateCells(datagrid_catalogo);
                row.Cells[0].Value = materiale.Abbreviazione ?? "";
                row.Cells[1].Value = materiale.Descrizione;
                row.Cells[2].Value = Convert.ToDecimal(materiale.Prezzo);
                row.Tag = materiale;
                return row;
            }).ToArray();

            datagrid_catalogo.Rows.AddRange(righe);
        }

        private void AggiornaLabelCount()
        {
            this.lbl_pezzi.Text = $"{datagrid_catalogo.Rows.Count - 1}";
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MaterialeEliminato(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Tag != null)
            {
                Materiale materiale = e.Row.Tag as Materiale;
                if (materiale.MaterialeLavoro.Count >= 1)
                {
                    MessageBox.Show("Non è possibile eliminare questo materiale in quanto già in uso.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    return;
                }
                this.materialiDaEliminare.Add(e.Row.Tag as Materiale);
                this.AggiornaLabelCount();
            }
        }

        private void RigaEliminataOAggiunta(object sender, DataGridViewRowEventArgs e)
        {
            this.AggiornaLabelCount();
        }

        /// <summary>
        /// Questo metodo controlla che nessuna altra riga della gestione listino contenga una descrizione o abbreviazione uguale alla cella correntemente editata, in modo da evitare duplicati
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConvalidaCella(object sender, DataGridViewCellValidatingEventArgs e)
        {
            // trattasi della quantità: convalidiamo solo se contiene effettivamente stringhe
            if (this.datagrid_catalogo.Columns[e.ColumnIndex].Name == "Prezzo")
                this.datagrid_catalogo.ValidateDecimalCell(e);

            if (this.datagrid_catalogo.Columns[e.ColumnIndex].Name == "Abbreviazione" || this.datagrid_catalogo.Columns[e.ColumnIndex].Name == "Descrizione")
            {
                string valoreCella = datagrid_catalogo.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().ToLower();
                if (valoreCella == string.Empty)
                    return;
                bool valoreGiaPresente = datagrid_catalogo.Rows.Cast<DataGridViewRow>().Any(row =>
                {
                    // se la riga non è quella corrente e c'è già qualche riga con la stessa abbreviazione o descrizione....
                    return row.Index != e.RowIndex && (valoreCella == row.Cells["Abbreviazione"].EditedFormattedValue.ToString().ToLower() || valoreCella == row.Cells["Descrizione"].EditedFormattedValue.ToString().ToLower());
                });
                if (valoreGiaPresente)
                {
                    MessageBox.Show("Non è possibile inserire più materiali con la stessa descrizione e/o abbreviazione", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    datagrid_catalogo.CancelEdit();
                    datagrid_catalogo.EndEdit();
                }
            }
        }

        private void datagrid_catalogo_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            /// Trasformazione di valori con . a ,    Necessario altrimenti la cultureinfo italiana trasforma 10.5 in 10500, in quanto prende il . come separatore delle migliaia!
            if (this.datagrid_catalogo.CurrentCell.ColumnIndex != 2)
                return;
            e.Control.KeyPress += (value, a) =>
            {
                if (this.datagrid_catalogo.CurrentCell.ColumnIndex == 2 && a.KeyChar == '.')
                    a.KeyChar = ',';
            };
        }

        // GESTIONE "FOCUS ALLA CELLA ADIACENTE" QUANDO SI INSERISCE UNA CELLA 
        private DataGridViewCell ultimaCellaEditata;
        // copiato da https://stackoverflow.com/a/9666741/1306679
        private void datagrid_catalogo_SelectionChanged(object sender, EventArgs e)
        {
            Utilities.NextCellFocusHandler((DataGridView)sender, ultimaCellaEditata);
        }
        private void datagrid_catalogo_modified(object sender, DataGridViewCellEventArgs e)
        {
            ultimaCellaEditata = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex]; // necessario per "spostamento" a cella successiva
            controllaNomiNonCorretti((DataGridView)sender, ultimaCellaEditata);
        }

        public void controllaNomiNonCorretti(DataGridView datagrid, DataGridViewCell cellaModificata)
        {
            bool valorePresente = cellaModificata.OwningRow.Cells[0].FormattedValue.ToString() != "" || cellaModificata.OwningRow.Cells[1].FormattedValue.ToString() != "";
            datagrid.Rows[cellaModificata.RowIndex].HeaderCell.Style.BackColor = valorePresente ? datagrid.RowHeadersDefaultCellStyle.BackColor : Color.OrangeRed;
        }
    }
}
