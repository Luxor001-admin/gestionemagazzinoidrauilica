﻿using MagazzinoIdraulica.Reports;
using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormStampaReport : Form
    {
        dbContext dbContext;
        List<Lavoro> lavoriDaStampare;
        Cantiere cantiereParent;
        public FormStampaReport(dbContext dbContext, List<Lavoro> lavoriDaStampare, Cantiere? cantiereParent)
        {
            this.dbContext = dbContext;
            this.lavoriDaStampare = lavoriDaStampare;
            this.cantiereParent = cantiereParent;
            InitializeComponent();
        }

        public FormStampaReport(dbContext dbContext, Lavoro lavoroDaStampare)
        {
            this.dbContext = dbContext;
            this.lavoriDaStampare = new List<Lavoro>() { lavoroDaStampare };
            this.cantiereParent = lavoroDaStampare.IdCantiereNavigation;
            InitializeComponent();

            // nascondo il campo della tipologia se è stato indicato di nasconderlo. questo accade ad esempio nella stampa di report singolo: che senso ha mostrare il filtro per tipologia per un singolo lavoro?
            this.cmb_tipologie.Visible = false;
            this.lbl_tipologia.Visible = false;
            this.chk_unisciLavori.Checked = false;
            this.chk_unisciLavori.Visible = false;
        }

        private void StampaReport_Load(object sender, EventArgs e)
        {
            var tipologie = new List<object>();
            tipologie.Add("Tutte le tipologie");
            tipologie.AddRange(dbContext.TipologiaLavoro);
            cmb_tipologie.Items.AddRange(tipologie.ToArray());
            cmb_tipologie.SelectedIndex = 0;
        }

        private void Stampa(object sender, EventArgs e)
        {
            // Prendiamo i lavori da stampare e filtriamoli in base alla tipologia
            var lavoriDaStampare = this.lavoriDaStampare;
            ReportOptions reportOptions = GetReportOptions();
            if (reportOptions.filtroTipologia != null)
                lavoriDaStampare = this.lavoriDaStampare.Where(lavoro => lavoro.IdTipologiaNavigation == reportOptions.filtroTipologia).ToList();

            ReportLavori report = new ReportLavori(lavoriDaStampare, reportOptions, this.cantiereParent);
            string pdfFilePath = Utilities.GetNewReportPath();
            report.GetReportTotali(pdfFilePath);
            System.Diagnostics.Process.Start(pdfFilePath);
        }

        /// <summary>
        /// Costruisce un reportOptions in base alle impostazioni selezionate e altre variabili
        /// </summary>
        /// <returns></returns>
        public ReportOptions GetReportOptions()
        {
            ReportOptions options = new ReportOptions();
            options.mostraImporti = chk_mostraImporti.Checked;
            options.mostraManodopera = chk_mostraManodopera.Checked;
            options.unisci = chk_unisciLavori.Checked;
            // controllo che l'utente non abbia lasciato il valore "tutte le tipologie" selezionato. in caso contrario, ottengo la tipologia di lavoro corrispondente
            if (this.cmb_tipologie.SelectedIndex != 0)
                options.filtroTipologia = this.cmb_tipologie.SelectedItem as TipologiaLavoro;
            return options;
        }

        private void Esci(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
