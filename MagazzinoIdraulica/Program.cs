﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    static class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // usiamo log4net per loggare il tutto su file
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler((sender,eventArgs) => {
                log.Error("\n\n////////////ECCEZIONE////////////", eventArgs.ExceptionObject as Exception);
            });

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Mainform(log));
        }
    }
}
