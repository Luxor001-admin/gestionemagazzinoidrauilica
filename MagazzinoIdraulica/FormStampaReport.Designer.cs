﻿namespace MagazzinoIdraulica
{
    partial class FormStampaReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_annulla = new System.Windows.Forms.Button();
            this.btn_stampa = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chk_unisciLavori = new System.Windows.Forms.CheckBox();
            this.lbl_tipologia = new System.Windows.Forms.Label();
            this.cmb_tipologie = new System.Windows.Forms.ComboBox();
            this.chk_mostraManodopera = new System.Windows.Forms.CheckBox();
            this.chk_mostraImporti = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_annulla
            // 
            this.btn_annulla.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_annulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_annulla.Location = new System.Drawing.Point(273, 162);
            this.btn_annulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_annulla.Name = "btn_annulla";
            this.btn_annulla.Size = new System.Drawing.Size(103, 34);
            this.btn_annulla.TabIndex = 4;
            this.btn_annulla.Text = "Annulla";
            this.btn_annulla.UseVisualStyleBackColor = true;
            this.btn_annulla.Click += new System.EventHandler(this.Esci);
            // 
            // btn_stampa
            // 
            this.btn_stampa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_stampa.Location = new System.Drawing.Point(164, 162);
            this.btn_stampa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_stampa.Name = "btn_stampa";
            this.btn_stampa.Size = new System.Drawing.Size(103, 34);
            this.btn_stampa.TabIndex = 3;
            this.btn_stampa.Text = "Stampa";
            this.btn_stampa.UseVisualStyleBackColor = true;
            this.btn_stampa.Click += new System.EventHandler(this.Stampa);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chk_unisciLavori);
            this.groupBox1.Controls.Add(this.lbl_tipologia);
            this.groupBox1.Controls.Add(this.cmb_tipologie);
            this.groupBox1.Controls.Add(this.chk_mostraManodopera);
            this.groupBox1.Controls.Add(this.chk_mostraImporti);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.groupBox1.Location = new System.Drawing.Point(0, 20);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(379, 137);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtri";
            // 
            // chk_unisciLavori
            // 
            this.chk_unisciLavori.AutoSize = true;
            this.chk_unisciLavori.Checked = true;
            this.chk_unisciLavori.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_unisciLavori.Location = new System.Drawing.Point(273, 68);
            this.chk_unisciLavori.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk_unisciLavori.Name = "chk_unisciLavori";
            this.chk_unisciLavori.Size = new System.Drawing.Size(99, 23);
            this.chk_unisciLavori.TabIndex = 5;
            this.chk_unisciLavori.Text = "unisci lavori";
            this.chk_unisciLavori.UseVisualStyleBackColor = true;
            // 
            // lbl_tipologia
            // 
            this.lbl_tipologia.AutoSize = true;
            this.lbl_tipologia.Location = new System.Drawing.Point(12, 26);
            this.lbl_tipologia.Name = "lbl_tipologia";
            this.lbl_tipologia.Size = new System.Drawing.Size(67, 19);
            this.lbl_tipologia.TabIndex = 4;
            this.lbl_tipologia.Text = "Tipologia:";
            this.lbl_tipologia.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmb_tipologie
            // 
            this.cmb_tipologie.FormattingEnabled = true;
            this.cmb_tipologie.Location = new System.Drawing.Point(85, 23);
            this.cmb_tipologie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_tipologie.Name = "cmb_tipologie";
            this.cmb_tipologie.Size = new System.Drawing.Size(288, 25);
            this.cmb_tipologie.TabIndex = 3;
            // 
            // chk_mostraManodopera
            // 
            this.chk_mostraManodopera.AutoSize = true;
            this.chk_mostraManodopera.Checked = true;
            this.chk_mostraManodopera.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_mostraManodopera.Location = new System.Drawing.Point(15, 95);
            this.chk_mostraManodopera.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk_mostraManodopera.Name = "chk_mostraManodopera";
            this.chk_mostraManodopera.Size = new System.Drawing.Size(153, 23);
            this.chk_mostraManodopera.TabIndex = 1;
            this.chk_mostraManodopera.Text = "mostra manodopera";
            this.chk_mostraManodopera.UseVisualStyleBackColor = true;
            // 
            // chk_mostraImporti
            // 
            this.chk_mostraImporti.AutoSize = true;
            this.chk_mostraImporti.Checked = true;
            this.chk_mostraImporti.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_mostraImporti.Location = new System.Drawing.Point(16, 68);
            this.chk_mostraImporti.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk_mostraImporti.Name = "chk_mostraImporti";
            this.chk_mostraImporti.Size = new System.Drawing.Size(119, 23);
            this.chk_mostraImporti.TabIndex = 0;
            this.chk_mostraImporti.Text = "mostra importi";
            // 
            // FormStampaReport
            // 
            this.AcceptButton = this.btn_stampa;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_annulla;
            this.ClientSize = new System.Drawing.Size(379, 207);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_annulla);
            this.Controls.Add(this.btn_stampa);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormStampaReport";
            this.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stampa Report";
            this.Load += new System.EventHandler(this.StampaReport_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_annulla;
        private System.Windows.Forms.Button btn_stampa;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_tipologia;
        private System.Windows.Forms.ComboBox cmb_tipologie;
        private System.Windows.Forms.CheckBox chk_mostraManodopera;
        private System.Windows.Forms.CheckBox chk_mostraImporti;
        private System.Windows.Forms.CheckBox chk_unisciLavori;
    }
}