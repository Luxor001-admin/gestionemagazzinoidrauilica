﻿using Common.Logging;
using iText.StyledXmlParser.Jsoup.Nodes;
using MagazzinoIdraulica.Code;
using MagazzinoIdraulica.Reports;
using MagazzinoIdraulicaDB.Models;
using Microsoft.EntityFrameworkCore;
using shortid.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class Mainform : Form
    {
        private dbContext dbContext;
        private log4net.ILog logger;

        public Mainform(log4net.ILog logger)
        {
            InitializeComponent();
            this.dbContext = new dbContext();
            this.logger = logger;

            this.immagini.Images.Add("icon_cantiere", Properties.Resources.icon_cantiere);
            this.immagini.Images.Add("icon_cliente", Properties.Resources.icon_cliente);

            this.dbContext.Database.EnsureCreated();
            this.migrationSteps();
            // Assicuriamoci che la cartella dei reports esista
            Directory.CreateDirectory($"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}\\reports");

            #if DEBUG
                this.btn_reinizializza.Visible = true;
#endif
        }

        private void migrationSteps()
        {
            try
            {
                this.dbContext.Database.ExecuteSqlRaw("ALTER TABLE \"Lavoro\" ADD \"Segnato\" tinyint NOT NULL DEFAULT 0;");
            }
            catch (Exception E)
            {

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            BuildClientiCantieriTree();
            // togliamo il focus iniziale dal button
            this.groupBox1.Select();
            this.btn_aggiungiLavoroTabCantiere.Enabled = false;

            // focussiamo il primo elemento disponibile della treeview
            TreeNode clienteConCantieri = tree_clientiCantieri.Nodes.Cast<TreeNode>().ToList().Find(node => node.Nodes.Count >= 1);
            if (clienteConCantieri != null)
            {
                tree_clientiCantieri.SelectedNode = clienteConCantieri.Nodes[0];
                tree_clientiCantieri.Select();
                tree_clientiCantieri.Focus();
                this.btn_aggiungiLavoroTabCantiere.Enabled = true;
                this.PopolaDataGridLavori(tree_clientiCantieri.SelectedNode.Tag as Cantiere);
            }
            this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
        }
        
        private void AggiungiLavoro(object sender, EventArgs e)
        {
            if (this.tree_clientiCantieri.SelectedNode == null || this.tree_clientiCantieri.SelectedNode.Tag as Cantiere == null)
                return;
            Cantiere cantiereCorrente = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            Lavoro nuovoLavoro = new Lavoro();
            nuovoLavoro.IdCantiereNavigation = cantiereCorrente;
            new FormOpenLavoro(dbContext, nuovoLavoro).ShowDialog();
            this.PopolaDataGridLavori(cantiereCorrente);
        }

        private void GestioneCatalogoClicked(object sender, EventArgs e)
        {
            FormGestioneListino gestioneListino = new FormGestioneListino(dbContext);
            gestioneListino.ShowDialog();
            if (tree_clientiCantieri.SelectedNode != null && tree_clientiCantieri.SelectedNode.Tag is Cantiere)
                this.PopolaDataGridLavori(tree_clientiCantieri.SelectedNode.Tag as Cantiere);
        }

        private void GestioneContrattiClicked(object sender, EventArgs e)
        {
            FormGestioneContratti gestioneContratti = new FormGestioneContratti(dbContext);
            gestioneContratti.ShowDialog();
            if (tree_clientiCantieri.SelectedNode != null && tree_clientiCantieri.SelectedNode.Tag is Cantiere)
                this.PopolaDataGridLavori(tree_clientiCantieri.SelectedNode.Tag as Cantiere);
        }

        private void GestioneTipologieClicked(object sender, EventArgs e)
        {
            FormGestioneTipologie tipologie = new FormGestioneTipologie(this.dbContext);
            tipologie.ShowDialog();
            if (tree_clientiCantieri.SelectedNode != null && tree_clientiCantieri.SelectedNode.Tag is Cantiere)
                this.PopolaDataGridLavori(tree_clientiCantieri.SelectedNode.Tag as Cantiere);
        }

        private void BuildClientiCantieriTree()
        {
            var oldNodeSelected = this.tree_clientiCantieri.SelectedNode;
            var oldTopNode = this.tree_clientiCantieri.TopNode;

            this.tree_clientiCantieri.Nodes.Clear();
            var nodi = this.dbContext.Cliente.Include("Cantiere").ToList().OrderBy(cliente => cliente.Nome).Select(cliente => this.CreateClienteNode(cliente)).ToArray();
            this.tree_clientiCantieri.Nodes.AddRange(nodi.ToArray());
            // se esisteva un nodo selezionato...
            if(oldNodeSelected != null)
            {
                // cerchiamo il "nuovo" nodo corrispondente al vecchio e selezioniamolo
                var newEqualNode = this.tree_clientiCantieri.Nodes.FlattenTree().FirstOrDefault(nodo => nodo.Tag == oldNodeSelected.Tag);
                if (newEqualNode != null)
                    this.tree_clientiCantieri.SelectedNode = newEqualNode;
                else
                { 
                    /* se invece non esiste (perché ad esempio cancellato) selezioniamo:
                     1) il cliente corrispondente se si trattava di un nodo cantiere
                     2) il primo cliente disponibile (se esistente) se si trattava di un nodo cliente
                    */
                    if (oldNodeSelected.Tag is Cantiere)
                        this.tree_clientiCantieri.SelectedNode = this.tree_clientiCantieri.Nodes.FlattenTree().FirstOrDefault(nodo => nodo.Tag == (oldNodeSelected.Tag as Cantiere).IdClienteNavigation);
                    if (oldNodeSelected.Tag is Cliente && this.tree_clientiCantieri.Nodes.Count >= 1)
                        this.tree_clientiCantieri.SelectedNode = this.tree_clientiCantieri.Nodes[0];
                }

                this.tree_clientiCantieri.TopNode = this.tree_clientiCantieri.SelectedNode;
            }
            this.tree_clientiCantieri.ExpandAll();

            // Reimpostiamo il nodo che era "top" all' inizio come top corrente
            if(oldTopNode != null)
            {
                var newEqualTopNode = this.tree_clientiCantieri.Nodes.FlattenTree().FirstOrDefault(nodo => nodo.Tag == oldTopNode.Tag);
                this.tree_clientiCantieri.TopNode = newEqualTopNode ?? this.tree_clientiCantieri.SelectedNode;
            }

            // reimposta anche il il nodo che era selezionato
            if (oldNodeSelected == null && this.tree_clientiCantieri.Nodes.Count >= 1 && this.tree_clientiCantieri.SelectedNode == null)
                this.tree_clientiCantieri.SelectedNode = this.tree_clientiCantieri.Nodes[0];
        }

        private TreeNode CreateClienteNode(Cliente cliente)
        {
            TreeNode nodoCliente = new TreeNode();
            nodoCliente.Tag = cliente;
            nodoCliente.Text = cliente.Nome;
            nodoCliente.Name = cliente.Id;
            nodoCliente.ContextMenuStrip = cnt_cliente;
            nodoCliente.ImageIndex = 1;
            nodoCliente.SelectedImageIndex = 1;
            nodoCliente.StateImageIndex = 1;
            nodoCliente.Text = nodoCliente.Text;

            var cantieriCliente = cliente.Cantiere.OrderBy(cantiere => cantiere.Nome).Select(cantiereCliente => this.CreateCantiereNode(cantiereCliente));
            nodoCliente.Nodes.AddRange(cantieriCliente.ToArray());
            return nodoCliente;
        }
        private TreeNode CreateCantiereNode(Cantiere cantiere)
        {
            TreeNode nodoCantiere = new TreeNode();
            nodoCantiere.Tag = cantiere;
            nodoCantiere.Text = cantiere.Nome;
            nodoCantiere.Name = cantiere.Id;
            nodoCantiere.ContextMenuStrip = cnt_cantiere;
            /* NOTA
               Dati i numerosi bug dei windows forms, ho dovuto impostare il treeview con un font di default a BOLD e impostare i node normali manualmente a regular
             * perché altrimenti incappavo in questo bug https://stackoverflow.com/questions/2272493/c-sharp-winforms-bold-treeview-node-doesnt-show-whole-text
             */
            nodoCantiere.NodeFont = new System.Drawing.Font(this.tree_clientiCantieri.Font, System.Drawing.FontStyle.Regular);
            return nodoCantiere;
        }

        private void PopolaDataGridLavori(Cantiere cantiereCorrente)
        {
            HashSet<string> lavoriSelezionati = righeLavoriDaSpostare.Select(row => (row.Tag as Lavoro).Id).ToHashSet();
            datagrid_lavori.Rows.Clear();
            var righe = cantiereCorrente.Lavoro.Select(lavoro =>
            {
                DataGridViewRow row = (DataGridViewRow)datagrid_lavori.RowTemplate.Clone();
                row.CreateCells(datagrid_lavori);
                long longVar = BitConverter.ToInt64(lavoro.Data, 0);
                row.Cells[0].Value = lavoro.Segnato;
                row.Cells[1].Value = new DateTime(longVar);
                row.Cells[2].Value = lavoro.IdTipologiaNavigation.Descrizione;
                row.Cells[3].Value = lavoro.Note;
                row.Cells[4].Value = lavoro.Referente;
                row.Cells[5].Value = lavoro.Indirizzo;
                row.Cells[6].Value = lavoro.calcolaTotale();
                // mostro eventuali lavori che sono in lista per essere spostati...
                if(lavoriSelezionati.Contains(lavoro.Id))
                    row.HeaderCell.Style.BackColor = Color.LightSlateGray;
                row.Tag = lavoro;
                return row;
            }).AsParallel().ToArray();

            datagrid_lavori.Rows.AddRange(righe);
            datagrid_lavori.Sort(datagrid_lavori.Columns[1], System.ComponentModel.ListSortDirection.Ascending);

            // queste operazioni sono necessarie per selezionare a prescindere la prima riga della tabella, in quanto il sort() potrebbe aver spostato la precedente prima riga della tabella
            if (datagrid_lavori.Rows.Count > 0)
            {
                datagrid_lavori.CurrentCell = datagrid_lavori.Rows[0].Cells[0];
            }
            datagrid_lavori.ClearSelection();
        }

        #region Gestione Cliente

        private void AggiungiCliente(object sender, EventArgs e)
        {
            FormAggiungiCliente formAggiungiCliente = new FormAggiungiCliente(this.dbContext);
            DialogResult dialogResult = formAggiungiCliente.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                TreeNode nuovoClienteNode = this.CreateClienteNode(formAggiungiCliente.cliente);
                List<string> nomiClienti = this.tree_clientiCantieri.Nodes.Cast<TreeNode>().ToList().Where(node => node.Tag is Cliente).Select(node => node.Text).ToList();
                nomiClienti.Add(nuovoClienteNode.Text);
                nomiClienti.Sort();
                this.tree_clientiCantieri.Nodes.Insert(nomiClienti.IndexOf(nuovoClienteNode.Text), nuovoClienteNode);
                this.tree_clientiCantieri.SelectedNode = tree_clientiCantieri.Nodes.Find(nuovoClienteNode.Name, false).First();
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
                this.UpdateDataGridLavori(tree_clientiCantieri.SelectedNode.Tag);
            }
        }

        private void RinominaCliente(object sender, EventArgs e)
        {
            FormAggiungiCliente formCliente = new FormAggiungiCliente(this.dbContext, this.tree_clientiCantieri.SelectedNode.Tag as Cliente);
            DialogResult result = formCliente.ShowDialog();
            if(formCliente.DialogResult == DialogResult.OK)
            {
                this.tree_clientiCantieri.SelectedNode.Text = formCliente.cliente.Nome;
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
            }
        }

        private void EliminaCliente(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Sei SICURO di voler eliminare questo cliente? Verranno eliminati anche tutti i cantieri e i lavori collegati", "Conferma necessaria", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.OK)
            {
                this.dbContext.Remove(this.tree_clientiCantieri.SelectedNode.Tag as Cliente);
                this.dbContext.SaveChanges();
                this.tree_clientiCantieri.Nodes.Remove(this.tree_clientiCantieri.SelectedNode);
                datagrid_lavori.Rows.Clear();
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
                this.UpdateDataGridLavori(tree_clientiCantieri.SelectedNode.Tag);
            }
        }

        #endregion

        #region Gestione Cantiere

        private void AggiungiCantiere(object sender, EventArgs e)
        {
            var nodo = this.tree_clientiCantieri.SelectedNode;
            FormAggiungiCantiere formAggiungiCantiere = new FormAggiungiCantiere(this.dbContext, (Cliente)nodo.Tag);
            DialogResult dialogResult = formAggiungiCantiere.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                TreeNode parentClienteNode = this.tree_clientiCantieri.Nodes.Find(((Cliente)nodo.Tag).Id, false).First();
                TreeNode cantiereNode = CreateCantiereNode(formAggiungiCantiere.cantiere);

                // Inserisce il cantiere all' interno del cliente e riordina la lista di cantieri
                List<TreeNode> treeNodes = parentClienteNode.Nodes.FlattenTree().ToList();
                treeNodes.Add(cantiereNode);
                treeNodes.Sort((x, y) => x.Text.CompareTo(y.Text));
                parentClienteNode.Nodes.Clear();
                parentClienteNode.Nodes.AddRange(treeNodes.ToArray());

                this.tree_clientiCantieri.SelectedNode = cantiereNode;
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
                this.UpdateDataGridLavori(tree_clientiCantieri.SelectedNode.Tag);
            }
        }

        private void RinominaCantiere(object sender, EventArgs e)
        {
            FormAggiungiCantiere formCantiere = new FormAggiungiCantiere(this.dbContext, this.tree_clientiCantieri.SelectedNode.Tag as Cantiere);
            DialogResult dialogResult = formCantiere.ShowDialog();
            if(dialogResult == DialogResult.OK)
            {
                this.tree_clientiCantieri.SelectedNode.Text = formCantiere.cantiere.Nome;
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
            }
        }

        private void SpostaCantiere(object sender, EventArgs e)
        {
            FormSpostaCantiere formSpostaCantiere = new FormSpostaCantiere(this.dbContext, this.tree_clientiCantieri.SelectedNode.Tag as Cantiere);
            DialogResult dialogResult = formSpostaCantiere.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                Cantiere cantiereSpostato = this.dbContext.Cantiere.Single(cantiere => cantiere.Id == formSpostaCantiere.cantiereDaSpostare.Id);

                this.tree_clientiCantieri.Nodes.Remove(this.tree_clientiCantieri.Nodes.Find(cantiereSpostato.Id, true).Single());
                datagrid_lavori.Rows.Clear();

                // Inserisce il cantiere all' interno del cliente e riordina la lista di cantieri
                TreeNode cantiereNode = CreateCantiereNode(cantiereSpostato);
                TreeNode clienteDestinazioneTreeNode = this.tree_clientiCantieri.Nodes.Find(formSpostaCantiere.clienteDestinazione.Id, false).First();
                List<TreeNode> treeNodes = clienteDestinazioneTreeNode.Nodes.FlattenTree().ToList();
                treeNodes.Add(cantiereNode);
                treeNodes.Sort((x, y) => x.Text.CompareTo(y.Text));
                clienteDestinazioneTreeNode.Nodes.Clear();
                clienteDestinazioneTreeNode.Nodes.AddRange(treeNodes.ToArray());


                this.tree_clientiCantieri.SelectedNode = tree_clientiCantieri.Nodes.Find(cantiereNode.Name, true).First();
                this.UpdateDataGridLavori(tree_clientiCantieri.SelectedNode.Tag);
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
            }
        }

        private void EliminaCantiere(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Sei SICURO di voler eliminare questo cantiere? Verranno eliminati anche tutti i lavori collegati", "Conferma necessaria", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.OK)
            {
                this.dbContext.Remove(this.tree_clientiCantieri.SelectedNode.Tag as Cantiere);
                this.dbContext.SaveChanges();
                this.tree_clientiCantieri.Nodes.Remove(this.tree_clientiCantieri.SelectedNode);
                datagrid_lavori.Rows.Clear();
                this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
                this.UpdateDataGridLavori(tree_clientiCantieri.SelectedNode.Tag);
            }
        }


        #endregion

        private void NodoSelezionato(object sender, TreeNodeMouseClickEventArgs e)
        {
            this.tree_clientiCantieri.SelectedNode = e.Node;
            this.sistemaTabRibbon(e.Node);
            UpdateDataGridLavori(e.Node.Tag);
        }

        private void UpdateDataGridLavori(object ClienteOCantiere)
        {
            if (ClienteOCantiere is Cantiere)
                this.PopolaDataGridLavori(ClienteOCantiere as Cantiere);
            else
                datagrid_lavori.Rows.Clear();
        }

        private void Reinizializza(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Sei SICURO di voler REINIZIALIZZARE TUTTO IL DB?\n PERDERAI TUTTI I DATI SALVATI\n L'AZIONE NON È REVERSIBILE", "Conferma necessaria", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.Cancel)
                return;
            this.dbContext.Database.EnsureDeleted();
            this.dbContext.Database.EnsureCreated();
            loadDemoData();
        }

        private void loadDemoData()
        {
            TipologiaLavoro nuovaTipologia = new TipologiaLavoro() { Id = Guid.NewGuid().ToString(), Descrizione = "Fognatura" };
            dbContext.TipologiaLavoro.Add(nuovaTipologia);
            dbContext.TipologiaLavoro.Add(new TipologiaLavoro() { Id = Guid.NewGuid().ToString(), Descrizione = "Manutenzione" });
            dbContext.TipologiaLavoro.Add(new TipologiaLavoro() { Id = Guid.NewGuid().ToString(), Descrizione = "Esterno" });
            dbContext.TipologiaLavoro.Add(new TipologiaLavoro() { Id = Guid.NewGuid().ToString(), Descrizione = "Interno" });

            dbContext.TipoContratto.Add(new TipoContratto() { Id = Guid.NewGuid().ToString(), Nome = "Apprendista", PagaOraria = 7 });
            dbContext.TipoContratto.Add(new TipoContratto() { Id = Guid.NewGuid().ToString(), Nome = "Operaio", PagaOraria = 10 });
            dbContext.TipoContratto.Add(new TipoContratto() { Id = Guid.NewGuid().ToString(), Nome = "Operaio esperto", PagaOraria = 12 });

            Materiale nuovoMateriale = new Materiale() { Id = Guid.NewGuid().ToString(), Descrizione = "Tubo da 15", Prezzo = 15 };
            dbContext.Materiale.Add(nuovoMateriale);
            dbContext.Materiale.Add(new Materiale() { Id = Guid.NewGuid().ToString(), Abbreviazione = "Tub 16", Descrizione = "Tubo da 16", Prezzo = 16 });
            dbContext.Materiale.Add(new Materiale() { Id = Guid.NewGuid().ToString(), Abbreviazione = "Tub 17", Descrizione = "Tubo da 17", Prezzo = 17 });
            dbContext.Materiale.Add(new Materiale() { Id = Guid.NewGuid().ToString(), Abbreviazione = "Tub 18", Descrizione = "Tubo da 18", Prezzo = 18 });

            Cliente nuovoCliente = new Cliente() { Id = Guid.NewGuid().ToString(), Nome = "Cliente di prova" };
            Cantiere nuovoCantiere = new Cantiere() { Id = Guid.NewGuid().ToString(), Nome = "Cantiere di prova", IdCliente = nuovoCliente.Id };
            Lavoro nuovoLavoro = new Lavoro() { Id = Guid.NewGuid().ToString(),  Indirizzo = "indirizzo di prova", IdCantiere = nuovoCantiere.Id, Data = BitConverter.GetBytes(DateTime.Now.Ticks), IdTipologia = nuovaTipologia.Id};
            MaterialeLavoro nuovoMaterialeLavoro = new MaterialeLavoro() { Id = Guid.NewGuid().ToString(), IdLavoro = nuovoLavoro.Id, IdMateriale = nuovoMateriale.Id, Quantita = 3};
            dbContext.Cliente.Add(nuovoCliente);
            dbContext.Cantiere.Add(nuovoCantiere);
            dbContext.Lavoro.Add(nuovoLavoro);
            dbContext.MaterialeLavoro.Add(nuovoMaterialeLavoro);
            dbContext.SaveChanges();
            this.BuildClientiCantieriTree();
            this.sistemaTabRibbon(tree_clientiCantieri.SelectedNode);
        }

        private void StampaCantiere(object sender, EventArgs e)
        {
            Cantiere cantiereSelezionato = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            FormStampaReport stampaReport = new FormStampaReport(this.dbContext, cantiereSelezionato.Lavoro.ToList(), cantiereSelezionato);
            stampaReport.ShowDialog();
        }

        private void LavoroDoppioClicckato(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            // PER GESTIONE CHECKBOX IN COLUMN, vedi https://stackoverflow.com/a/15011844/1306679
            if (e.ColumnIndex == Segnato_column.Index && e.RowIndex != -1)
            {
                this.datagrid_lavori.EndEdit();
                return;
            }

            Lavoro lavoro = this.datagrid_lavori.Rows[e.RowIndex].Tag as Lavoro;
            new FormOpenLavoro(dbContext, lavoro).ShowDialog();
            this.PopolaDataGridLavori(lavoro.IdCantiereNavigation);
        }

        private void StampaLavoriSelezionati(object sender, EventArgs e)
        {
            Cantiere cantiereSelezionato = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            List<Lavoro> lavoriSelezionati = datagrid_lavori.SelectedRows.Cast<DataGridViewRow>().Select(row => row.Tag as Lavoro).ToList();
            FormStampaReport stampaReport = new FormStampaReport(this.dbContext, lavoriSelezionati, cantiereSelezionato);
            stampaReport.ShowDialog();
        }

        private void cnt_lavoro_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.datagrid_lavori.SelectedRows.Count == 0)
            {
                var rows = datagrid_lavori.Rows.Cast<DataGridViewRow>().Select(row => row.HeaderCell);
                var contains = rows.FirstOrDefault(row => row.ContentBounds.Contains(ClientRectangle));
                if (contains != null)
                    datagrid_lavori.Rows[contains.RowIndex].Selected = true;
                else
                    e.Cancel = true;
            }
        }

        /// <summary>
        /// Metodo che "sistema" i tab del ribbon in base al nodo selezionato
        /// </summary>
        /// <param name="nodoSelezionato">Il nodo selezionato da considerare. DEVE essere passato come parametro (e non inferito da treeview) perché NON è garantito che treeview.selectedNode sia sempre aggiornato all'ultima azione utente (ad es. al click utente)</param>
        private void sistemaTabRibbon(TreeNode nodoSelezionato)
        {
            this.tab_cliente.Enabled = false;
            this.tab_cantiere.Enabled = false;
            this.btn_aggiungiLavoroTabCantiere.Enabled = false;
            if (nodoSelezionato == null)
            {
                this.ribbon.ActiveTab = this.tab_home;
                return;
            }

            if (nodoSelezionato.Tag is Cantiere)
            {
                this.tab_cantiere.Enabled = true;
                this.ribbon.ActiveTab = this.tab_cantiere;
                this.btn_aggiungiLavoroTabCantiere.Enabled = true;
            }
            else
            {
                this.tab_cliente.Enabled = true;
                this.ribbon.ActiveTab = this.tab_cliente;
            }
        }

        private void Esci(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FormInChiusura(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.dbContext.DoDailyBackup();
            }
            catch(Exception ex)
            {
                this.logger.Error(ex);
            }
        }

        /// <summary>
        /// Metodo chiamato ad ogni apertura del button "STAMPA".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StampaDropDownShowing(object sender, EventArgs e)
        {
            // se ci sono elementi selezionati nella datagrid, abilita il pulsante
            this.btn_stampaSelezionati.Enabled = datagrid_lavori.SelectedRows.Count >= 1;
        }

        // caso in cui l'utente usa "CANC"
        private void datagrid_lavori_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue != 46)
                return;
            string messaggio = datagrid_lavori.SelectedRows.Count == 1 ? "Sei SICURO di voler eliminare questo lavoro selezionato?" : "Sei SICURO di voler eliminare questo lavori selezionati?";
            DialogResult result = MessageBox.Show(messaggio, "Conferma necessaria", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.Cancel)
            {
                e.Handled = true;
                return;
            }
            // da qui in poi partirà un evento EventoDataGridEliminaLavoroDaCanc, uno per riga
        }

        // questo evento viene chiamato più volte per ogni riga. Viene invocato dalla datagrid SOLO nel caso in cui l'eliminazione avvenga tramite CANC.
        private void EventoDataGridEliminaLavoroDaCanc(object sender, DataGridViewRowCancelEventArgs e)
        {
            Cantiere cantiereSelezionato = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            this.dbContext.Lavoro.Remove(e.Row.Tag as Lavoro);
            this.dbContext.SaveChangesAsync();
        }

        // caso in cui l'utente cliccka "cancella righe" dal tooltip
        private void cancellaLavoriSelezionatiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string messaggio = datagrid_lavori.SelectedRows.Count == 1 ? "Sei SICURO di voler eliminare questo lavoro selezionato?" : "Sei SICURO di voler eliminare questo lavori selezionati?";
            DialogResult result = MessageBox.Show(messaggio, "Conferma necessaria", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.Cancel)
                return;

            // devo gestire manualmente la cancellazione, in quanto la datagrid non invocherà EventoDataGridEliminaLavoroDaCanc
            datagrid_lavori.SelectedRows.Cast<DataGridViewRow>().ToList().ForEach(row => {
                this.dbContext.Lavoro.Remove(row.Tag as Lavoro);
                this.datagrid_lavori.Rows.Remove(row);
            });
            this.dbContext.SaveChanges();
        }

        // PER GESTIONE CHECKBOX IN COLUMN, vedi https://stackoverflow.com/a/15011844/1306679
        private void datagrid_lavori_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Segnato_column.Index && e.RowIndex != -1)
            {
                Lavoro lavoro = this.datagrid_lavori.Rows[e.RowIndex].Tag as Lavoro;
                lavoro.Segnato = (bool)this.datagrid_lavori.Rows[e.RowIndex].Cells[0].Value;
                dbContext.SaveChanges();
            }
        }

        // PER GESTIONE CHECKBOX IN COLUMN, vedi https://stackoverflow.com/a/15011844/1306679
        private void datagrid_lavori_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == Segnato_column.Index && e.RowIndex != -1)
            {
                this.datagrid_lavori.EndEdit();
            }
        }

        private void ImpostaSegnati(object sender, EventArgs e)
        {
            Cantiere cantiereSelezionato = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            List<Lavoro> lavoriSelezionati = datagrid_lavori.SelectedRows.Cast<DataGridViewRow>().Select(row => row.Tag as Lavoro).ToList();
            lavoriSelezionati.ForEach(lavoro => lavoro.Segnato = true);
            dbContext.SaveChanges();
            this.PopolaDataGridLavori(cantiereSelezionato);
        }

        private void RimuoviSegnati(object sender, EventArgs e)
        {
            Cantiere cantiereSelezionato = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            List<Lavoro> lavoriSelezionati = datagrid_lavori.SelectedRows.Cast<DataGridViewRow>().Select(row => row.Tag as Lavoro).ToList();
            lavoriSelezionati.ForEach(lavoro => lavoro.Segnato = false);
            dbContext.SaveChanges();
            this.PopolaDataGridLavori(cantiereSelezionato);
        }

        private void txt_cercaCliente_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cercaClientePerNomeESelezionaNodo(this.txt_cercaCliente.Text);
            }
        }
        private void btn_cercaCliente_Click(object sender, EventArgs e)
        {
            this.cercaClientePerNomeESelezionaNodo(this.txt_cercaCliente.Text);
        }

        private void cercaClientePerNomeESelezionaNodo(string nomeCliente)
        {
            TreeNode nodoCliente = this.tree_clientiCantieri.Nodes.Cast<TreeNode>().ToList()
                .Where(node => node.Tag is Cliente)
                .FirstOrDefault(node => node.Text == nomeCliente);
            if (nodoCliente != null)
            {
                nodoCliente.EnsureVisible();
                this.tree_clientiCantieri.TopNode = nodoCliente;
                this.tree_clientiCantieri.SelectedNode = nodoCliente;
                this.UpdateDataGridLavori(this.tree_clientiCantieri.SelectedNode);
                this.tree_clientiCantieri.Focus();
            }
        }

        private void txt_cercaCliente_focus(object sender, EventArgs e)
        {
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            source.AddRange(this.tree_clientiCantieri.Nodes.Cast<TreeNode>().Select(node => node.Text).ToArray());
            txt_cercaCliente.AutoCompleteCustomSource = source;
        }

        #region Gestione spostamento lavori
        List<DataGridViewRow> righeLavoriDaSpostare = new List<DataGridViewRow>();
        private void SelezionaLavoriDaSpostare_click(object sender, EventArgs e)
        {
            // resettiamo lo stile delle vecchie righe da spostare...
            this.righeLavoriDaSpostare.ForEach(riga => riga.HeaderCell.Style = datagrid_lavori.RowHeadersDefaultCellStyle);

            this.righeLavoriDaSpostare = datagrid_lavori.SelectedRows.Cast<DataGridViewRow>().ToList();
            this.righeLavoriDaSpostare.ForEach(riga => riga.HeaderCell.Style.BackColor = Color.LightSlateGray);
            datagrid_lavori.ClearSelection();
        }

        /**
         * Evento invocato quando l'utente fa click destro su un cantiere
         * */
        private void cnt_cantiere_opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int nLavori = righeLavoriDaSpostare.Count;
            Cantiere cantiereDestinazione = this.tree_clientiCantieri.SelectedNode.Tag as Cantiere;
            // se qualche lavoro è stato selezionato e il cantiere di destinazione è diverso da quello di partenza...
            if (nLavori > 0 && cantiereDestinazione.Id != (righeLavoriDaSpostare[0].Tag as Lavoro).IdCantiereNavigation.Id)
            {
                this.tlstp_spostaLavoriSelezionati.Visible = true;
                this.tlstrp_separator_sposta.Visible = true;
                this.tlstp_spostaLavoriSelezionati.Text = nLavori == 1
                    ? "Sposta il lavoro selezionato qui"
                    : $"Sposta i {this.righeLavoriDaSpostare.Count} lavori selezionati qui";
            }
            else
            {
                this.tlstp_spostaLavoriSelezionati.Visible = false;
                this.tlstrp_separator_sposta.Visible = false;
            }
        }

        /**
         * Evento invocato quando l'utente decide di clicckare "sposta" su un cantiere
         * */
        private void SpostaLavoriSelezionati_click(object sender, EventArgs e)
        {
            HashSet<string> lavoriSelezionati = righeLavoriDaSpostare.Select(row => (row.Tag as Lavoro).Id).ToHashSet();

            var lavoriDaSpostare = this.dbContext.Lavoro.Where(l => lavoriSelezionati.Contains(l.Id)).ToList();
            var cantiereSelezionato = this.dbContext.Cantiere.Include(e => e.Lavoro).Single(e => e.Id == (this.tree_clientiCantieri.SelectedNode.Tag as Cantiere).Id);
            var vecchioCantiere = this.dbContext.Cantiere.Include(e => e.Lavoro).Single(e => e.Id == lavoriDaSpostare[0].IdCantiereNavigation.Id);
            lavoriDaSpostare.ForEach((lavoro) => cantiereSelezionato.Lavoro.Add(lavoro));
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges(true);
            lavoriDaSpostare.ForEach((lavoro) => vecchioCantiere.Lavoro.Remove(lavoro));
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges(true);

            this.UpdateDataGridLavori(this.tree_clientiCantieri.SelectedNode.Tag);
            this.righeLavoriDaSpostare.Clear();
            this.righeLavoriDaSpostare.ForEach(riga => riga.HeaderCell.Style = datagrid_lavori.RowHeadersDefaultCellStyle);
        }

        #endregion

    }
}
