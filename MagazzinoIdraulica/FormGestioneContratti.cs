﻿using MagazzinoIdraulica.Code;
using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormGestioneContratti : Form
    {
        dbContext dbContext;
        List<TipoContratto> contrattiEliminati = new List<TipoContratto>();
        public FormGestioneContratti(dbContext dbContext)
        {
            this.dbContext = dbContext;
            InitializeComponent();
            this.datagrid_contratti.Columns["PagaOraria"].DefaultCellStyle.Format = "c";
            this.datagrid_contratti.Columns["PagaOraria"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("it-it");
            this.datagrid_contratti.Columns["PagaOraria"].ValueType = Type.GetType("System.Decimal");
        }

        private void GestioneContratti_Load(object sender, EventArgs e)
        {
            PopolaDataGrid(this.dbContext.TipoContratto);
        }

        private void PopolaDataGrid(IEnumerable<TipoContratto> contratti)
        {
            var righe = contratti.Select(contratto =>
            {
                DataGridViewRow row = (DataGridViewRow)datagrid_contratti.RowTemplate.Clone();
                row.CreateCells(datagrid_contratti);
                row.Cells[0].Value = contratto.Nome;
                row.Cells[1].Value = contratto.PagaOraria;
                row.Tag = contratto;
                return row;
            }).ToArray();
            datagrid_contratti.Rows.AddRange(righe);
        }

        private void Salva(object sender, EventArgs e)
        {
            datagrid_contratti.Rows.Cast<DataGridViewRow>()
                .Where(row => !row.IsNewRow)
                .Where(row => !string.IsNullOrEmpty(row.Cells["Tipologia"].EditedFormattedValue.ToString())) // ignoriamo tutte le righe con campo principale vuoto
                .ToList()
                .ForEach(rowContratti =>
                {
                    TipoContratto contratto = (TipoContratto)rowContratti.Tag ?? new TipoContratto();
                    contratto.Nome = rowContratti.Cells["Tipologia"].EditedFormattedValue.ToString();
                    contratto.PagaOraria = rowContratti.Cells["PagaOraria"].ValueToDecimal();
                    if (rowContratti.Tag != null)
                        dbContext.Entry(contratto).CurrentValues.SetValues(contratto);
                    else
                        dbContext.TipoContratto.Add(contratto);
                }); 

            dbContext.TipoContratto.RemoveRange(this.contrattiEliminati);
            dbContext.SaveChanges();
            this.Close();
        }

        private void ContrattoEliminato(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Tag != null)
            {
                TipoContratto contratto = e.Row.Tag as TipoContratto;
                if (contratto.Prestazione.Count >= 1)
                {
                    MessageBox.Show("Non è possibile eliminare questo contratto in quanto già in uso.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                    return;
                }
                this.contrattiEliminati.Add(e.Row.Tag as TipoContratto);
            }
        }

        private void Esci(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConvalidaCella(object sender, DataGridViewCellValidatingEventArgs e)
        {
            // trattasi della quantità: convalidiamo solo se contiene effettivamente stringhe
            if (this.datagrid_contratti.Columns[e.ColumnIndex].Name == "PagaOraria")
            {

                this.datagrid_contratti.ValidateDecimalCell(e);
            }

            if (this.datagrid_contratti.Columns[e.ColumnIndex].Name == "Tipologia")
            {
                string valoreCella = this.datagrid_contratti.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString();
                if (valoreCella == string.Empty)
                    return;
                bool valoreGiaPresente = this.datagrid_contratti.Rows.Cast<DataGridViewRow>().Any(row =>
                {
                    // se la riga non è quella corrente e c'è già qualche riga con la stessa tipologia....
                    return row.Index != e.RowIndex && (valoreCella.ToLower() == row.Cells["Tipologia"].EditedFormattedValue.ToString().ToLower());
                });
                if (valoreGiaPresente)
                {
                    MessageBox.Show("Non è possibile inserire più contratti con lo stesso nome", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    datagrid_contratti.CancelEdit();
                    datagrid_contratti.EndEdit();
                }
            }
        }

        private void datagrid_contratti_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (this.datagrid_contratti.CurrentCell.ColumnIndex != 1)
                return;
            e.Control.KeyPress += (value, a) =>
            {
                if (this.datagrid_contratti.CurrentCell.ColumnIndex == 1 && a.KeyChar == '.')
                    a.KeyChar = ',';
            };
        }

        // GESTIONE "FOCUS ALLA CELLA ADIACENTE" QUANDO SI INSERISCE UNA CELLA 
        private DataGridViewCell ultimaCellaEditata;
        private void datagrid_contratti_SelectionChanged(object sender, EventArgs e)
        {
            Utilities.NextCellFocusHandler((DataGridView)sender, ultimaCellaEditata);
        }
        private void datagrid_contratti_modified(object sender, DataGridViewCellEventArgs e)
        {
            ultimaCellaEditata = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex]; // necessario per "spostamento" a cella successiva
            controllaNomiNonCorretti((DataGridView)sender, ultimaCellaEditata);
        }

        public void controllaNomiNonCorretti(DataGridView datagrid, DataGridViewCell cellaModificata)
        {
            bool valorePresente = cellaModificata.OwningRow.Cells[0].FormattedValue.ToString() != "";
            datagrid.Rows[cellaModificata.RowIndex].HeaderCell.Style.BackColor = valorePresente ? datagrid.RowHeadersDefaultCellStyle.BackColor : Color.OrangeRed;
        }
    }
}
