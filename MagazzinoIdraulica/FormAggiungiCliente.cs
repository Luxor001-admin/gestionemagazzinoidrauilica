﻿using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormAggiungiCliente : Form
    {
        private dbContext dbContext;
        public Cliente cliente;
        public FormAggiungiCliente(dbContext dbContext, Cliente cliente = null)
        {
            this.dbContext = dbContext;
            this.InitializeComponent();
            if (cliente != null)
            {
                this.cliente = cliente;
                this.txt_nomeCliente.Text = cliente.Nome;
                this.Text = "Modifica cliente";
            }
        }

        private void Salva(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_nomeCliente.Text))
            {
                MessageBox.Show("Nome di cliente vuoto", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.dbContext.Cliente.Any(cliente => cliente.Nome == txt_nomeCliente.Text && this.cliente != cliente)) {
                MessageBox.Show("Non è possibile inserire più clienti con lo stesso nome", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.cliente != null)
            {
                this.cliente.Nome = txt_nomeCliente.Text;
                this.dbContext.Entry(this.cliente).CurrentValues.SetValues(this.cliente);
            }
            else
            {
                this.cliente = new Cliente(txt_nomeCliente.Text);
                this.dbContext.Cliente.Add(this.cliente);
            }
            this.dbContext.SaveChanges();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Annulla(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
