﻿using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormSpostaCantiere : Form
    {
        private dbContext dbContext;
        public Cantiere cantiereDaSpostare;
        public Cliente clienteDestinazione = null;
        private Dictionary<string, Cliente> altriClienti = null;

        public FormSpostaCantiere(dbContext dbContext, Cantiere cantiereDaSpostare)
        {
            this.dbContext = dbContext;
            this.cantiereDaSpostare = cantiereDaSpostare;
            this.InitializeComponent();
        }

        private void FormSpostaCantiere_Load(object sender, EventArgs e)
        {
            // seleziono tutti gli altri cliente ad eccezione di quello corrente.
            this.altriClienti = dbContext.Cliente.ToList()
                .Where(cliente => cliente.Nome != cantiereDaSpostare.IdClienteNavigation.Nome)
                .Aggregate(new Dictionary<string, Cliente>(), (dictionary, cliente) =>
            {
                dictionary.Add(cliente.Nome, cliente);
                return dictionary;
            });
            cmb_nomeCliente.Items.AddRange(altriClienti.Keys.ToArray());
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            source.AddRange(altriClienti.Keys.ToArray());
            cmb_nomeCliente.AutoCompleteCustomSource = source;
            this.btn_salva.Enabled = false;
        }

        private void Salva(object sender, EventArgs e)
        {
            Cliente clienteOrigine = dbContext.Cliente.Single(cliente => cliente.Id == cantiereDaSpostare.IdClienteNavigation.Id);
            Cliente clienteDestinazione = dbContext.Cliente.Single(cliente => cliente.Nome == cmb_nomeCliente.Text);

            clienteDestinazione.Cantiere.Add(this.cantiereDaSpostare);
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges(true);
            clienteOrigine.Cantiere.Remove(cantiereDaSpostare);
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges(true);

            this.clienteDestinazione = clienteDestinazione;
            this.dbContext.SaveChanges();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Annulla(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmb_nomeCliente_textChanged(object sender, EventArgs e)
        {
            this.btn_salva.Enabled = this.altriClienti.ContainsKey(cmb_nomeCliente.Text);
        }
    }
}
