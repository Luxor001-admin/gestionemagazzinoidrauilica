﻿namespace MagazzinoIdraulica
{
    partial class FormOpenLavoro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.datapicker_data = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_tipologie = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_indirizzo = new System.Windows.Forms.TextBox();
            this.txt_referente = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Fornitura = new System.Windows.Forms.TabPage();
            this.datagrid_materiali = new System.Windows.Forms.DataGridView();
            this.Materiale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_prestazioni = new System.Windows.Forms.TabPage();
            this.datagrid_prestazioni = new System.Windows.Forms.DataGridView();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Minuti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chk_stampato = new System.Windows.Forms.CheckBox();
            this.txt_note = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_totale = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_manodopera = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_fornitura = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pnl_separatore = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stampaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listinoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnl_totali = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl_dati = new System.Windows.Forms.Panel();
            this.pnl_griglia = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.Fornitura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_materiali)).BeginInit();
            this.tab_prestazioni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_prestazioni)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnl_totali.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnl_dati.SuspendLayout();
            this.pnl_griglia.SuspendLayout();
            this.SuspendLayout();
            // 
            // datapicker_data
            // 
            this.datapicker_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.datapicker_data.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datapicker_data.Location = new System.Drawing.Point(88, 48);
            this.datapicker_data.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.datapicker_data.Name = "datapicker_data";
            this.datapicker_data.Size = new System.Drawing.Size(120, 26);
            this.datapicker_data.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(234, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tipologia:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmb_tipologie
            // 
            this.cmb_tipologie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tipologie.Location = new System.Drawing.Point(309, 47);
            this.cmb_tipologie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_tipologie.Name = "cmb_tipologie";
            this.cmb_tipologie.Size = new System.Drawing.Size(196, 28);
            this.cmb_tipologie.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Indirizzo:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Referente:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_indirizzo
            // 
            this.txt_indirizzo.Location = new System.Drawing.Point(88, 86);
            this.txt_indirizzo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_indirizzo.Name = "txt_indirizzo";
            this.txt_indirizzo.Size = new System.Drawing.Size(417, 26);
            this.txt_indirizzo.TabIndex = 2;
            // 
            // txt_referente
            // 
            this.txt_referente.Location = new System.Drawing.Point(88, 119);
            this.txt_referente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_referente.Name = "txt_referente";
            this.txt_referente.Size = new System.Drawing.Size(417, 26);
            this.txt_referente.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Fornitura);
            this.tabControl1.Controls.Add(this.tab_prestazioni);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(519, 313);
            this.tabControl1.TabIndex = 10;
            // 
            // Fornitura
            // 
            this.Fornitura.Controls.Add(this.datagrid_materiali);
            this.Fornitura.Location = new System.Drawing.Point(4, 29);
            this.Fornitura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Fornitura.Name = "Fornitura";
            this.Fornitura.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Fornitura.Size = new System.Drawing.Size(511, 280);
            this.Fornitura.TabIndex = 0;
            this.Fornitura.Text = "Fornitura";
            this.Fornitura.UseVisualStyleBackColor = true;
            // 
            // datagrid_materiali
            // 
            this.datagrid_materiali.AllowUserToResizeColumns = false;
            this.datagrid_materiali.AllowUserToResizeRows = false;
            this.datagrid_materiali.ColumnHeadersHeight = 30;
            this.datagrid_materiali.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.datagrid_materiali.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Materiale,
            this.Quantita});
            this.datagrid_materiali.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagrid_materiali.EnableHeadersVisualStyles = false;
            this.datagrid_materiali.Location = new System.Drawing.Point(3, 2);
            this.datagrid_materiali.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.datagrid_materiali.Name = "datagrid_materiali";
            this.datagrid_materiali.RowHeadersWidth = 40;
            this.datagrid_materiali.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.datagrid_materiali.RowTemplate.Height = 24;
            this.datagrid_materiali.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.datagrid_materiali.Size = new System.Drawing.Size(505, 276);
            this.datagrid_materiali.TabIndex = 11;
            this.datagrid_materiali.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.materiali_prestazioni_modified);
            this.datagrid_materiali.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ConvalidaCella);
            this.datagrid_materiali.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.datagrid_materiali_EditingControlShowing);
            this.datagrid_materiali.SelectionChanged += new System.EventHandler(this.materiali_prestazioni_SelectionChanged);
            this.datagrid_materiali.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.MaterialeLavoroEliminato);
            // 
            // Materiale
            // 
            this.Materiale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Materiale.FillWeight = 80F;
            this.Materiale.HeaderText = "Materiale";
            this.Materiale.MinimumWidth = 6;
            this.Materiale.Name = "Materiale";
            this.Materiale.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Quantita
            // 
            this.Quantita.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Format = "N0";
            this.Quantita.DefaultCellStyle = dataGridViewCellStyle7;
            this.Quantita.FillWeight = 20F;
            this.Quantita.HeaderText = "Quantita";
            this.Quantita.MinimumWidth = 6;
            this.Quantita.Name = "Quantita";
            this.Quantita.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // tab_prestazioni
            // 
            this.tab_prestazioni.Controls.Add(this.datagrid_prestazioni);
            this.tab_prestazioni.Location = new System.Drawing.Point(4, 29);
            this.tab_prestazioni.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tab_prestazioni.Name = "tab_prestazioni";
            this.tab_prestazioni.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tab_prestazioni.Size = new System.Drawing.Size(511, 280);
            this.tab_prestazioni.TabIndex = 1;
            this.tab_prestazioni.Text = "Manodopera";
            this.tab_prestazioni.UseVisualStyleBackColor = true;
            // 
            // datagrid_prestazioni
            // 
            this.datagrid_prestazioni.AllowUserToResizeColumns = false;
            this.datagrid_prestazioni.AllowUserToResizeRows = false;
            this.datagrid_prestazioni.ColumnHeadersHeight = 30;
            this.datagrid_prestazioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.datagrid_prestazioni.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo,
            this.Ore,
            this.Minuti});
            this.datagrid_prestazioni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagrid_prestazioni.EnableHeadersVisualStyles = false;
            this.datagrid_prestazioni.Location = new System.Drawing.Point(3, 2);
            this.datagrid_prestazioni.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.datagrid_prestazioni.Name = "datagrid_prestazioni";
            this.datagrid_prestazioni.RowHeadersWidth = 40;
            this.datagrid_prestazioni.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.datagrid_prestazioni.RowTemplate.Height = 24;
            this.datagrid_prestazioni.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.datagrid_prestazioni.Size = new System.Drawing.Size(505, 276);
            this.datagrid_prestazioni.TabIndex = 1;
            this.datagrid_prestazioni.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.materiali_prestazioni_modified);
            this.datagrid_prestazioni.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ConvalidaCella);
            this.datagrid_prestazioni.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.datagrid_manodopera_EditingControlShowing);
            this.datagrid_prestazioni.SelectionChanged += new System.EventHandler(this.materiali_prestazioni_SelectionChanged);
            this.datagrid_prestazioni.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.PrestazioneLavoroEliminata);
            // 
            // Tipo
            // 
            this.Tipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Tipo.FillWeight = 80F;
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.MinimumWidth = 6;
            this.Tipo.Name = "Tipo";
            this.Tipo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Ore
            // 
            this.Ore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = "0";
            this.Ore.DefaultCellStyle = dataGridViewCellStyle8;
            this.Ore.FillWeight = 10F;
            this.Ore.HeaderText = "Ore";
            this.Ore.MinimumWidth = 6;
            this.Ore.Name = "Ore";
            this.Ore.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Minuti
            // 
            this.Minuti.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = "0";
            this.Minuti.DefaultCellStyle = dataGridViewCellStyle9;
            this.Minuti.FillWeight = 10F;
            this.Minuti.HeaderText = "Minuti";
            this.Minuti.MinimumWidth = 6;
            this.Minuti.Name = "Minuti";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chk_stampato);
            this.groupBox1.Controls.Add(this.txt_note);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.datapicker_data);
            this.groupBox1.Controls.Add(this.txt_referente);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_indirizzo);
            this.groupBox1.Controls.Add(this.cmb_tipologie);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(519, 226);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // chk_stampato
            // 
            this.chk_stampato.AutoSize = true;
            this.chk_stampato.Location = new System.Drawing.Point(417, 18);
            this.chk_stampato.Name = "chk_stampato";
            this.chk_stampato.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chk_stampato.Size = new System.Drawing.Size(102, 24);
            this.chk_stampato.TabIndex = 9;
            this.chk_stampato.Text = "Stampato";
            this.chk_stampato.UseVisualStyleBackColor = true;
            this.chk_stampato.CheckedChanged += new System.EventHandler(this.chk_stampato_CheckedChanged);
            // 
            // txt_note
            // 
            this.txt_note.Location = new System.Drawing.Point(88, 155);
            this.txt_note.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_note.Multiline = true;
            this.txt_note.Name = "txt_note";
            this.txt_note.Size = new System.Drawing.Size(417, 59);
            this.txt_note.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 155);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Note:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_totale);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txt_manodopera);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txt_fornitura);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(10, 10);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(211, 125);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Totale";
            // 
            // txt_totale
            // 
            this.txt_totale.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txt_totale.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Bold);
            this.txt_totale.Location = new System.Drawing.Point(101, 89);
            this.txt_totale.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_totale.Name = "txt_totale";
            this.txt_totale.ReadOnly = true;
            this.txt_totale.Size = new System.Drawing.Size(97, 27);
            this.txt_totale.TabIndex = 13;
            this.txt_totale.TabStop = false;
            this.txt_totale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "TOTALE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_manodopera
            // 
            this.txt_manodopera.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txt_manodopera.Font = new System.Drawing.Font("Consolas", 10F);
            this.txt_manodopera.Location = new System.Drawing.Point(101, 55);
            this.txt_manodopera.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_manodopera.Name = "txt_manodopera";
            this.txt_manodopera.ReadOnly = true;
            this.txt_manodopera.Size = new System.Drawing.Size(97, 27);
            this.txt_manodopera.TabIndex = 16;
            this.txt_manodopera.TabStop = false;
            this.txt_manodopera.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Manodopera";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_fornitura
            // 
            this.txt_fornitura.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txt_fornitura.Font = new System.Drawing.Font("Consolas", 10F);
            this.txt_fornitura.Location = new System.Drawing.Point(100, 23);
            this.txt_fornitura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_fornitura.Name = "txt_fornitura";
            this.txt_fornitura.ReadOnly = true;
            this.txt_fornitura.Size = new System.Drawing.Size(98, 27);
            this.txt_fornitura.TabIndex = 15;
            this.txt_fornitura.TabStop = false;
            this.txt_fornitura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Fornitura";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnl_separatore
            // 
            this.pnl_separatore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_separatore.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_separatore.Location = new System.Drawing.Point(0, 0);
            this.pnl_separatore.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnl_separatore.Name = "pnl_separatore";
            this.pnl_separatore.Size = new System.Drawing.Size(519, 2);
            this.pnl_separatore.TabIndex = 13;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.visualizzaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(519, 31);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvaToolStripMenuItem,
            this.stampaToolStripMenuItem,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(49, 27);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // salvaToolStripMenuItem
            // 
            this.salvaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.salva;
            this.salvaToolStripMenuItem.Name = "salvaToolStripMenuItem";
            this.salvaToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.salvaToolStripMenuItem.Text = "Salva";
            this.salvaToolStripMenuItem.Click += new System.EventHandler(this.Salva);
            // 
            // stampaToolStripMenuItem
            // 
            this.stampaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.stampa;
            this.stampaToolStripMenuItem.Name = "stampaToolStripMenuItem";
            this.stampaToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.stampaToolStripMenuItem.Text = "Stampa...";
            this.stampaToolStripMenuItem.Click += new System.EventHandler(this.Stampa);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.Esci);
            // 
            // visualizzaToolStripMenuItem
            // 
            this.visualizzaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listinoToolStripMenuItem});
            this.visualizzaToolStripMenuItem.Name = "visualizzaToolStripMenuItem";
            this.visualizzaToolStripMenuItem.Size = new System.Drawing.Size(98, 27);
            this.visualizzaToolStripMenuItem.Text = "Visualizza";
            this.visualizzaToolStripMenuItem.Visible = false;
            // 
            // listinoToolStripMenuItem
            // 
            this.listinoToolStripMenuItem.Name = "listinoToolStripMenuItem";
            this.listinoToolStripMenuItem.Size = new System.Drawing.Size(143, 28);
            this.listinoToolStripMenuItem.Text = "Listino";
            this.listinoToolStripMenuItem.Click += new System.EventHandler(this.MostraListino);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(519, 313);
            this.panel2.TabIndex = 15;
            // 
            // pnl_totali
            // 
            this.pnl_totali.Controls.Add(this.panel1);
            this.pnl_totali.Controls.Add(this.pnl_separatore);
            this.pnl_totali.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_totali.Location = new System.Drawing.Point(0, 570);
            this.pnl_totali.Name = "pnl_totali";
            this.pnl_totali.Size = new System.Drawing.Size(519, 137);
            this.pnl_totali.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10, 10, 0, 0);
            this.panel1.Size = new System.Drawing.Size(519, 135);
            this.panel1.TabIndex = 14;
            // 
            // pnl_dati
            // 
            this.pnl_dati.Controls.Add(this.groupBox1);
            this.pnl_dati.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_dati.Location = new System.Drawing.Point(0, 31);
            this.pnl_dati.Name = "pnl_dati";
            this.pnl_dati.Size = new System.Drawing.Size(519, 226);
            this.pnl_dati.TabIndex = 17;
            // 
            // pnl_griglia
            // 
            this.pnl_griglia.Controls.Add(this.panel2);
            this.pnl_griglia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_griglia.Location = new System.Drawing.Point(0, 257);
            this.pnl_griglia.Name = "pnl_griglia";
            this.pnl_griglia.Size = new System.Drawing.Size(519, 313);
            this.pnl_griglia.TabIndex = 18;
            // 
            // FormOpenLavoro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 707);
            this.Controls.Add(this.pnl_griglia);
            this.Controls.Add(this.pnl_dati);
            this.Controls.Add(this.pnl_totali);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormOpenLavoro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lavoro";
            this.Load += new System.EventHandler(this.MostraLavoro_Load);
            this.tabControl1.ResumeLayout(false);
            this.Fornitura.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_materiali)).EndInit();
            this.tab_prestazioni.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_prestazioni)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnl_totali.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.pnl_dati.ResumeLayout(false);
            this.pnl_griglia.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker datapicker_data;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmb_tipologie;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_indirizzo;
        private System.Windows.Forms.TextBox txt_referente;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Fornitura;
        private System.Windows.Forms.DataGridView datagrid_materiali;
        private System.Windows.Forms.TabPage tab_prestazioni;
        private System.Windows.Forms.DataGridView datagrid_prestazioni;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_manodopera;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_fornitura;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnl_separatore;
        private System.Windows.Forms.TextBox txt_totale;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stampaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.TextBox txt_note;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem visualizzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listinoToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Materiale;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantita;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnl_totali;
        private System.Windows.Forms.Panel pnl_dati;
        private System.Windows.Forms.Panel pnl_griglia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Minuti;
        private System.Windows.Forms.CheckBox chk_stampato;
    }
}