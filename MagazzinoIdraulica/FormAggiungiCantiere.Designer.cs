﻿namespace MagazzinoIdraulica
{
    partial class FormAggiungiCantiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_annulla = new System.Windows.Forms.Button();
            this.btn_salva = new System.Windows.Forms.Button();
            this.txt_nomeCantiere = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_annulla
            // 
            this.btn_annulla.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_annulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.btn_annulla.Location = new System.Drawing.Point(218, 94);
            this.btn_annulla.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_annulla.Name = "btn_annulla";
            this.btn_annulla.Size = new System.Drawing.Size(103, 34);
            this.btn_annulla.TabIndex = 2;
            this.btn_annulla.Text = "Annulla";
            this.btn_annulla.UseVisualStyleBackColor = true;
            this.btn_annulla.Click += new System.EventHandler(this.Annulla);
            // 
            // btn_salva
            // 
            this.btn_salva.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.btn_salva.Location = new System.Drawing.Point(109, 94);
            this.btn_salva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_salva.Name = "btn_salva";
            this.btn_salva.Size = new System.Drawing.Size(103, 34);
            this.btn_salva.TabIndex = 1;
            this.btn_salva.Text = "Salva";
            this.btn_salva.UseVisualStyleBackColor = true;
            this.btn_salva.Click += new System.EventHandler(this.Salva);
            // 
            // txt_nomeCantiere
            // 
            this.txt_nomeCantiere.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nomeCantiere.Location = new System.Drawing.Point(15, 39);
            this.txt_nomeCantiere.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_nomeCantiere.Name = "txt_nomeCantiere";
            this.txt_nomeCantiere.Size = new System.Drawing.Size(307, 23);
            this.txt_nomeCantiere.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nome cantiere";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FormAggiungiCantiere
            // 
            this.AcceptButton = this.btn_salva;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_annulla;
            this.ClientSize = new System.Drawing.Size(337, 142);
            this.Controls.Add(this.txt_nomeCantiere);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_annulla);
            this.Controls.Add(this.btn_salva);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormAggiungiCantiere";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aggiungi Cantiere";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_annulla;
        private System.Windows.Forms.Button btn_salva;
        private System.Windows.Forms.TextBox txt_nomeCantiere;
        private System.Windows.Forms.Label label3;
    }
}