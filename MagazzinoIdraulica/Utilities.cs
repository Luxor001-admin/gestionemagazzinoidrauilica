﻿
using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public static class Utilities
    {
        /// <summary>
        /// Converte un double nella sua rapparesentazione stringa con simbolo della valuta
        /// </summary>
        /// <param name="importo"></param>
        /// <returns></returns>
        public static string GetImportoCurrency(decimal importo)
        {
            // tronchiamo tutto ciò che è dopo le due cifre decimali...
            importo = Convert.ToDecimal(String.Format("{0:0.00}", importo));
            CultureInfo cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            cultureInfo.NumberFormat.CurrencyPositivePattern = 3;
            cultureInfo.NumberFormat.CurrencyNegativePattern = 3;
            return string.Format(cultureInfo, "{0:C2}", importo);
        }


        public static string GetNewReportPath()
        {
            return $"{System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}\\reports\\{DateTime.Now.ToShortDateString().Replace('/', '-')}_{Guid.NewGuid().ToString().Remove(6)}.pdf";
        }

        /// <summary>
        /// Metodo che muove il focus nella cella successiva, preso allegramente da https://stackoverflow.com/a/9666741/1306679
        /// </summary>
        /// <param name="datagridView"></param>
        /// <param name="ultimaCellaEditata"></param>
        public static void NextCellFocusHandler(DataGridView datagridView, DataGridViewCell ultimaCellaEditata)
        {
            if (Control.MouseButtons != 0) return;

            if (ultimaCellaEditata != null && datagridView.CurrentCell != null)
            {
                // if we are currently in the next line of last edit cell
                if (datagridView.CurrentCell.RowIndex == ultimaCellaEditata.RowIndex + 1 &&
                    datagridView.CurrentCell.ColumnIndex == ultimaCellaEditata.ColumnIndex)
                {
                    int iColNew;
                    int iRowNew = 0;
                    if (ultimaCellaEditata.ColumnIndex >= datagridView.ColumnCount - 1)
                    {
                        iColNew = 0;
                        iRowNew = datagridView.CurrentCell.RowIndex;
                    }
                    else
                    {
                        iColNew = ultimaCellaEditata.ColumnIndex + 1;
                        iRowNew = ultimaCellaEditata.RowIndex;
                    }
                    datagridView.CurrentCell = datagridView[iColNew, iRowNew];
                }
            }
            ultimaCellaEditata = null;
        }
    }
}
