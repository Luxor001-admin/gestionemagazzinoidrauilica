﻿using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MagazzinoIdraulica
{
    public partial class FormAggiungiCantiere : Form
    {
        private dbContext dbContext;
        private Cliente clienteParent;
        public Cantiere cantiere;
        public FormAggiungiCantiere(dbContext dbContext, Cliente clienteParent)
        {
            this.dbContext = dbContext;
            this.clienteParent = clienteParent;
            this.InitializeComponent();
        }

        public FormAggiungiCantiere(dbContext dbContext, Cantiere cantiereDaModificare)
        {
            this.dbContext = dbContext;
            this.cantiere = cantiereDaModificare;
            this.InitializeComponent();
            this.txt_nomeCantiere.Text = cantiereDaModificare.Nome;
            this.Text = "Modifica cantiere";
        }

        private void Salva(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_nomeCantiere.Text))
            {
                MessageBox.Show("Nome di cantiere vuoto", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(cantiere != null)
            {
                this.cantiere.Nome = this.txt_nomeCantiere.Text;
                this.dbContext.Entry(cantiere).CurrentValues.SetValues(cantiere);
            }
            else
            {
                Cantiere cantiere = new Cantiere(txt_nomeCantiere.Text, this.clienteParent.Id);
                this.dbContext.Cantiere.Add(cantiere);
                this.cantiere = cantiere;
            }
            this.dbContext.SaveChanges();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Annulla(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
