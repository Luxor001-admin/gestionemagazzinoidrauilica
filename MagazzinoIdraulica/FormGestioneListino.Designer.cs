﻿using System;

namespace MagazzinoIdraulica
{
    partial class FormGestioneListino
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.datagrid_catalogo = new System.Windows.Forms.DataGridView();
            this.Abbreviazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrizione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prezzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ricerca = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbl_pezzi = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_catalogo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // datagrid_catalogo
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.datagrid_catalogo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.datagrid_catalogo.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.datagrid_catalogo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.datagrid_catalogo.ColumnHeadersHeight = 30;
            this.datagrid_catalogo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.datagrid_catalogo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Abbreviazione,
            this.Descrizione,
            this.Prezzo});
            this.datagrid_catalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagrid_catalogo.EnableHeadersVisualStyles = false;
            this.datagrid_catalogo.Location = new System.Drawing.Point(0, 0);
            this.datagrid_catalogo.Margin = new System.Windows.Forms.Padding(4);
            this.datagrid_catalogo.Name = "datagrid_catalogo";
            this.datagrid_catalogo.RowHeadersWidth = 40;
            this.datagrid_catalogo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.datagrid_catalogo.RowTemplate.Height = 24;
            this.datagrid_catalogo.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.datagrid_catalogo.Size = new System.Drawing.Size(884, 902);
            this.datagrid_catalogo.TabIndex = 2;
            this.datagrid_catalogo.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagrid_catalogo_modified);
            this.datagrid_catalogo.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ConvalidaCella);
            this.datagrid_catalogo.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.datagrid_catalogo_EditingControlShowing);
            this.datagrid_catalogo.SelectionChanged += new System.EventHandler(this.datagrid_catalogo_SelectionChanged);
            this.datagrid_catalogo.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.RigaEliminataOAggiunta);
            this.datagrid_catalogo.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.RigaEliminataOAggiunta);
            this.datagrid_catalogo.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.MaterialeEliminato);
            // 
            // Abbreviazione
            // 
            this.Abbreviazione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Abbreviazione.FillWeight = 25F;
            this.Abbreviazione.HeaderText = "Abbreviazione";
            this.Abbreviazione.MinimumWidth = 6;
            this.Abbreviazione.Name = "Abbreviazione";
            // 
            // Descrizione
            // 
            this.Descrizione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descrizione.FillWeight = 60F;
            this.Descrizione.HeaderText = "Descrizione";
            this.Descrizione.MinimumWidth = 6;
            this.Descrizione.Name = "Descrizione";
            this.Descrizione.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Prezzo
            // 
            this.Prezzo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = "0,00 €";
            this.Prezzo.DefaultCellStyle = dataGridViewCellStyle2;
            this.Prezzo.FillWeight = 15F;
            this.Prezzo.HeaderText = "Prezzo";
            this.Prezzo.MinimumWidth = 6;
            this.Prezzo.Name = "Prezzo";
            this.Prezzo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(884, 27);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvaToolStripMenuItem,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 23);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // salvaToolStripMenuItem
            // 
            this.salvaToolStripMenuItem.Image = global::MagazzinoIdraulica.Properties.Resources.salva;
            this.salvaToolStripMenuItem.Name = "salvaToolStripMenuItem";
            this.salvaToolStripMenuItem.Size = new System.Drawing.Size(113, 26);
            this.salvaToolStripMenuItem.Text = "Salva";
            this.salvaToolStripMenuItem.Click += new System.EventHandler(this.Salva);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(113, 26);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.esciToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(884, 934);
            this.panel1.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.datagrid_catalogo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 32);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(884, 902);
            this.panel3.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(884, 32);
            this.panel2.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.txt_ricerca);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(839, 32);
            this.panel5.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cerca";
            // 
            // txt_ricerca
            // 
            this.txt_ricerca.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt_ricerca.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txt_ricerca.Location = new System.Drawing.Point(59, 7);
            this.txt_ricerca.Margin = new System.Windows.Forms.Padding(4);
            this.txt_ricerca.Name = "txt_ricerca";
            this.txt_ricerca.Size = new System.Drawing.Size(425, 23);
            this.txt_ricerca.TabIndex = 1;
            this.txt_ricerca.TextChanged += new System.EventHandler(this.TestoRicercaCambiato);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbl_pezzi);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(839, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(45, 32);
            this.panel4.TabIndex = 7;
            // 
            // lbl_pezzi
            // 
            this.lbl_pezzi.AutoSize = true;
            this.lbl_pezzi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl_pezzi.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl_pezzi.Location = new System.Drawing.Point(0, 15);
            this.lbl_pezzi.Margin = new System.Windows.Forms.Padding(4, 30, 4, 0);
            this.lbl_pezzi.Name = "lbl_pezzi";
            this.lbl_pezzi.Size = new System.Drawing.Size(24, 17);
            this.lbl_pezzi.TabIndex = 5;
            this.lbl_pezzi.Text = "10";
            this.lbl_pezzi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FormGestioneListino
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 961);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormGestioneListino";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestione Listino";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.GestioneCatalogo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.datagrid_catalogo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView datagrid_catalogo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_ricerca;
        private System.Windows.Forms.Label lbl_pezzi;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Abbreviazione;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrizione;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prezzo;
    }
}