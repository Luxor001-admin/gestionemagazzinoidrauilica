﻿using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using MagazzinoIdraulica.Code;
using MagazzinoIdraulicaDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MagazzinoIdraulica.Reports
{
    class ReportLavori
    {
        private List<Lavoro> lavoriDaStampare;
        private ReportOptions options;
        private Cantiere cantiere;
        public ReportLavori(List<Lavoro> lavoriDaStampare, ReportOptions options, Cantiere cantiereParent)
        {
            this.lavoriDaStampare = lavoriDaStampare;
            this.options = options ?? new ReportOptions();
            this.cantiere = cantiereParent;
        }

        public void GetReportTotali(string path)
        {
            PdfWriter writer = new PdfWriter(path);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);
            document.SetTopMargin(20);

            //// DATA ////
            Paragraph data = new Paragraph(DateTime.Now.ToString("dd/MM/yyyy"));
            data.SetTextAlignment(TextAlignment.RIGHT);
            document.Add(data);

            //// TITOLO ////
            Paragraph header = new Paragraph("Report cantiere")
               .SetTextAlignment(TextAlignment.CENTER)
               .SetFontSize(20);
            document.Add(header);
            document.Add(new Paragraph("\n"));

            //// CLIENTE + TIPO FILTRO ////
            Paragraph p = new Paragraph($"{this.cantiere.IdClienteNavigation.Nome}");
            p.Add(new Tab());
            p.AddTabStops(new TabStop(1000, TabAlignment.RIGHT));
            p.Add($"FILTRO: {this.options.filtroTipologia?.Descrizione ?? "Tutte le tipologie"}");
            document.Add(p);

            //// CANTIERE ////
            Paragraph Cantiere = new Paragraph($"{this.cantiere.Nome}")
               .SetTextAlignment(TextAlignment.LEFT);
            p.Add(new Tab());
            document.Add(Cantiere);
            document.Add(new Paragraph("\n"));

            if (this.options.unisci)
            {
                Table tabellaMateriali = BuildTabellaMateriali2(this.lavoriDaStampare, this.options);
                document.Add(tabellaMateriali);

                if (this.options.mostraManodopera)
                {
                    document.Add(new Paragraph("\n"));
                    Table tabellaPrestazioni = BuildTabellaPrestazioni(this.lavoriDaStampare, this.options);
                    document.Add(tabellaPrestazioni);
                }

                if (this.options.mostraImporti)
                {
                    document.Add(new Paragraph("\n"));
                    Table tabellaTotali = this.BuildTabellaTotali(this.lavoriDaStampare, this.options);
                    document.Add(tabellaTotali);
                }
            }

            if (!this.options.unisci)
            {
                this.lavoriDaStampare.ForEach(lavoro =>
                {
                    if (this.lavoriDaStampare.IndexOf(lavoro) != 0)
                    {
                        document.Add(new AreaBreak());
                        document.Add(new Paragraph("\n\n\n\n\n"));
                    }
                    else
                    {
                        document.Add(new Paragraph("\n"));
                        LineSeparator separator = new LineSeparator(new DashedLine(1f));
                        document.Add(separator);
                        document.Add(new Paragraph("\n"));
                    }
                    document.Add(this.BuildTableDescrizioneLavoro(lavoro));
                    document.Add(new Paragraph("\n"));
                    Table tabellaMateriali = BuildTabellaMateriali2(new List<Lavoro>() { lavoro }, this.options);
                    document.Add(tabellaMateriali);

                    if (this.options.mostraManodopera)
                    {
                        document.Add(new Paragraph("\n"));
                        Table tabellaPrestazioni = BuildTabellaPrestazioni(new List<Lavoro>() { lavoro }, this.options);
                        document.Add(tabellaPrestazioni);
                    }

                    if (this.options.mostraImporti)
                    {
                        document.Add(new Paragraph("\n"));
                        Table tabellaTotali = this.BuildTabellaTotali(new List<Lavoro>() { lavoro }, this.options);
                        document.Add(tabellaTotali);
                    }
                });

                // non mostro il totale dei totali se le pagine da stampare sono solo 1!
                if(this.lavoriDaStampare.Count > 1)
                {

                    // Mostro il totale dei totali
                    document.Add(new AreaBreak());
                    document.Add(new Paragraph("\n"));

                    header = new Paragraph("Totale")
                       .SetTextAlignment(TextAlignment.CENTER)
                       .SetBold()
                       .SetFontSize(17);
                    document.Add(header);

                    document.Add(new Paragraph("\n\n\n\n"));

                    Table tabellaMateriali = BuildTabellaMateriali2(this.lavoriDaStampare, this.options);
                    document.Add(tabellaMateriali);
                    document.Add(new Paragraph("\n"));

                    if (this.options.mostraManodopera)
                    {
                        Table tabellaPrestazioni = BuildTabellaPrestazioni(this.lavoriDaStampare, this.options);
                        document.Add(tabellaPrestazioni);
                        document.Add(new Paragraph("\n"));
                    }

                    if (this.options.mostraImporti)
                    {
                        document.Add(new Paragraph("\n"));
                        Table tabellaTotali = this.BuildTabellaTotali(this.lavoriDaStampare, this.options);
                        document.Add(tabellaTotali);
                    }
                }
            }

            document.Close();
        }


        /// <summary>
        /// Questo metodo crea una tabella di materiali "tipica": ogni lavoro è listato uno sopra l'altro e i materiali non sono raggruppati
        /// </summary>
        /// <returns></returns>
        private Table BuildTabellaMateriali()
        {
            decimal quantitaTotale = 0;
            decimal importoTotale = 0;

            // Table
            float[] columnsWidth = this.options.mostraImporti ? new float[] { 400, 100, 100 } : new float[] { 400, 100 };
            Table tabellaMateriali = new Table(columnsWidth);
            Cell HeaderDescrizione = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245)))
               .Add(new Paragraph("Descrizione"));
            Cell HeaderQuantita = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245)))
               .Add(new Paragraph("Quantita"));

            Cell HeaderImporto = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245)))
               .Add(new Paragraph("Importo"));

            // this.cantiere = this.dbContext.Cantiere.Include(c => c.Lavoro).Where(c => c.Id == this.cantiere.Id).First();
            var celleLavoro = this.lavoriDaStampare.Select(lavoro =>
            {
                var righeMateriali = lavoro.MaterialeLavoro.Select(materialeUtilizzato =>
                {
                    List<Cell> rigaMateriale = new List<Cell>();
                    Cell cellaDescrizione = new Cell(1, 1);
                    cellaDescrizione.Add(new Paragraph(materialeUtilizzato.IdMaterialeNavigation.Descrizione));
                    cellaDescrizione.SetTextAlignment(TextAlignment.LEFT);
                    cellaDescrizione.SetPaddingLeft(10);
                    rigaMateriale.Add(cellaDescrizione);

                    Cell cellaQuantita = new Cell(1, 1);
                    quantitaTotale += materialeUtilizzato.Quantita;
                    cellaQuantita.Add(new Paragraph(materialeUtilizzato.Quantita.ToString()));
                    cellaQuantita.SetTextAlignment(TextAlignment.RIGHT);
                    cellaQuantita.SetPaddingRight(5);
                    rigaMateriale.Add(cellaQuantita);

                    if (this.options.mostraImporti)
                    {
                        decimal prezzo = materialeUtilizzato.Quantita * materialeUtilizzato.IdMaterialeNavigation.Prezzo;
                        importoTotale += prezzo;
                        Cell cellaPrezzo = new Cell(1, 1);
                        cellaPrezzo.Add(new Paragraph(Utilities.GetImportoCurrency(prezzo)));
                        cellaPrezzo.SetTextAlignment(TextAlignment.RIGHT);
                        cellaPrezzo.SetPaddingRight(5);
                        rigaMateriale.Add(cellaPrezzo);
                    }

                    return rigaMateriale;
                }).SelectMany(x => x).ToList(); // SelectMany === flatten
                return righeMateriali;
            }).SelectMany(x => x).ToList();

            tabellaMateriali.AddHeaderCell(HeaderDescrizione);
            tabellaMateriali.AddHeaderCell(HeaderQuantita);
            if (this.options.mostraImporti)
                tabellaMateriali.AddHeaderCell(HeaderImporto);
            // AGGIUNTA RIGHE MATERIALI (O RIGA VUOTA)
            if (celleLavoro.ToList().Count >= 1)
                celleLavoro.ToList().ForEach(cella => tabellaMateriali.AddCell(cella));
            else
            {
                Cell cellaVuota = new Cell(1, 3);
                cellaVuota.SetTextAlignment(TextAlignment.CENTER);
                cellaVuota.SetItalic();
                cellaVuota.Add(new Paragraph("Nessun materiale da visualizzare"));
                tabellaMateriali.AddCell(cellaVuota);
            }

            Cell FooterTOTALE = new Cell(1, 1);
            FooterTOTALE.SetTextAlignment(TextAlignment.CENTER);
            FooterTOTALE.SetBackgroundColor(Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245)));
            FooterTOTALE.SetBold();
            FooterTOTALE.Add(new Paragraph("TOTALE"));

            Cell FooterTOTALEQuantita = new Cell(1, 1);
            FooterTOTALEQuantita.SetTextAlignment(TextAlignment.RIGHT);
            FooterTOTALEQuantita.Add(new Paragraph(quantitaTotale.ToString()));
            FooterTOTALEQuantita.SetBold();
            FooterTOTALEQuantita.SetBackgroundColor(Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245)));
            FooterTOTALEQuantita.SetPaddingRight(5);

            Cell FooterTOTALEImporto = new Cell(1, 1);
            FooterTOTALEImporto.SetTextAlignment(TextAlignment.RIGHT);
            FooterTOTALEImporto.Add(new Paragraph(Utilities.GetImportoCurrency(importoTotale)));
            FooterTOTALEImporto.SetBold();
            FooterTOTALEImporto.SetBackgroundColor(Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245)));
            FooterTOTALEImporto.SetPaddingRight(5);

            tabellaMateriali.AddFooterCell(FooterTOTALE);
            tabellaMateriali.AddFooterCell(FooterTOTALEQuantita);
            if (this.options.mostraImporti)
                tabellaMateriali.AddFooterCell(FooterTOTALEImporto);

            return tabellaMateriali;
        }

        private Table BuildTabellaMateriali2(List<Lavoro> lavori, ReportOptions reportOptions)
        {
            decimal quantitaTotale = 0;
            decimal importoTotale = 0;

            // Table
            Color backgroundHeader = Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245));
            float[] columnsWidth = reportOptions.mostraImporti ? new float[] { 400, 100, 100 } : new float[] { 400, 100 };
            Table tabellaMateriali = new Table(columnsWidth);

            Cell HeaderIntestazione = new Cell(1, 3)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBold()
               .SetFontSize(13)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Materiali"));
            Cell HeaderDescrizione = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Descrizione"));
            Cell HeaderQuantita = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Quantita"));

            Cell HeaderImporto = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Importo"));

            // faccio un GROUP BY di tutti i materiali usati nei lavori listati. in questo modo posso fare la sum di tutte le quantità per gruppo
            var materialiLavoroUtilizzati = lavori
                .Select(lavoro => lavoro.MaterialeLavoro) // per ogni lavoro, otteniamo i materiali usati
                .SelectMany(x => x) // facciamo un flatmap che è un array di array 
                .GroupBy(materialeLavoro => materialeLavoro.IdMaterialeNavigation) // raggruppiamo per Materiale, in modo da riuscire a sommare il tutto
                .Select(materialeLavoroGrouped => new MaterialeUsato(materialeLavoroGrouped.Key, materialeLavoroGrouped.Sum(m => m.Quantita)));

            var righeMateriali = materialiLavoroUtilizzati.Select(materialeUtilizzato =>
            {
                List<Cell> rigaMateriale = new List<Cell>();
                Cell cellaDescrizione = new Cell(1, 1);
                cellaDescrizione.Add(new Paragraph(materialeUtilizzato.materiale.Descrizione));
                cellaDescrizione.SetTextAlignment(TextAlignment.LEFT);
                cellaDescrizione.SetPaddingLeft(10);
                rigaMateriale.Add(cellaDescrizione);

                Cell cellaQuantita = new Cell(1, 1);
                quantitaTotale += materialeUtilizzato.quantita;
                cellaQuantita.Add(new Paragraph(materialeUtilizzato.quantita.ToString()));
                cellaQuantita.SetTextAlignment(TextAlignment.RIGHT);
                cellaQuantita.SetPaddingRight(5);
                rigaMateriale.Add(cellaQuantita);

                if (reportOptions.mostraImporti)
                {
                    decimal prezzo = materialeUtilizzato.quantita * materialeUtilizzato.materiale.Prezzo;
                    importoTotale += prezzo;
                    Cell cellaPrezzo = new Cell(1, 1);
                    cellaPrezzo.Add(new Paragraph(Utilities.GetImportoCurrency(prezzo)));
                    cellaPrezzo.SetTextAlignment(TextAlignment.RIGHT);
                    cellaPrezzo.SetPaddingRight(5);
                    rigaMateriale.Add(cellaPrezzo);
                }

                return rigaMateriale;
            }).SelectMany(x => x).ToList(); // SelectMany === flatten

            tabellaMateriali.AddCell(HeaderIntestazione);
            tabellaMateriali.AddCell(HeaderDescrizione);
            tabellaMateriali.AddCell(HeaderQuantita);
            if (reportOptions.mostraImporti)
                tabellaMateriali.AddCell(HeaderImporto);

            // AGGIUNTA RIGHE MATERIALI (O RIGA VUOTA)
            if (righeMateriali.ToList().Count >= 1)
                righeMateriali.ToList().ForEach(cella => tabellaMateriali.AddCell(cella));
            else
            {
                Cell cellaVuota = new Cell(1, 3);
                cellaVuota.SetTextAlignment(TextAlignment.CENTER);
                cellaVuota.SetItalic();
                cellaVuota.Add(new Paragraph("Nessun materiale da visualizzare"));
                tabellaMateriali.AddCell(cellaVuota);
            }

            Cell FooterTOTALE = new Cell(1, 1);
            FooterTOTALE.SetTextAlignment(TextAlignment.CENTER);
            FooterTOTALE.SetBackgroundColor(backgroundHeader);
            FooterTOTALE.SetBold();
            FooterTOTALE.Add(new Paragraph("TOTALE"));

            Cell FooterTOTALEQuantita = new Cell(1, 1);
            FooterTOTALEQuantita.SetTextAlignment(TextAlignment.RIGHT);
            FooterTOTALEQuantita.Add(new Paragraph(quantitaTotale.ToString()));
            FooterTOTALEQuantita.SetBold();
            FooterTOTALEQuantita.SetBackgroundColor(backgroundHeader);
            FooterTOTALEQuantita.SetPaddingRight(5);

            Cell FooterTOTALEImporto = new Cell(1, 1);
            FooterTOTALEImporto.SetTextAlignment(TextAlignment.RIGHT);
            FooterTOTALEImporto.Add(new Paragraph(Utilities.GetImportoCurrency(importoTotale)));
            FooterTOTALEImporto.SetBold();
            FooterTOTALEImporto.SetBackgroundColor(backgroundHeader);
            FooterTOTALEImporto.SetPaddingRight(5);

            tabellaMateriali.AddCell(FooterTOTALE);
            tabellaMateriali.AddCell(FooterTOTALEQuantita);
            if (reportOptions.mostraImporti)
                tabellaMateriali.AddCell(FooterTOTALEImporto);

            tabellaMateriali.AddHeaderCell(new Cell(1, 3)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetItalic().
                SetFontSize(10).
                Add(new Paragraph("Continua da pagina precedente...")));
            tabellaMateriali.SetSkipFirstHeader(true);
            return tabellaMateriali;
        }


        private Table BuildTabellaPrestazioni(List<Lavoro> lavori, ReportOptions reportOptions)
        {
            decimal importoTotale = 0;
            decimal tempoTotale = 0;

            // Table
            Color backgroundHeader = Color.ConvertRgbToCmyk(new DeviceRgb(245, 245, 245));
            float[] columnsWidth = reportOptions.mostraImporti ? new float[] { 400, 100, 100 } : new float[] { 400, 100 };
            Table tabellaPrestazioni = new Table(columnsWidth);
            Cell HeaderIntestazione = new Cell(1, 3)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBold()
               .SetFontSize(13)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Manodopera"));
            Cell HeaderDescrizione = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Descrizione"));
            Cell HeaderTempo = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Tempo"));

            Cell headerCosto = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBackgroundColor(backgroundHeader)
               .Add(new Paragraph("Costo"));

            // faccio un GROUP BY di tutti i materiali usati nei lavori listati. in questo modo posso fare la sum di tutte le quantità per gruppo
            var prestazioniSegnate = lavori
                .Select(lavoro => lavoro.Prestazione) // per ogni lavoro, otteniamo i materiali usati
                .SelectMany(x => x) // facciamo un flatmap che è un array di array 
                .GroupBy(prestazione => prestazione.IdContrattoNavigation) // raggruppiamo per Materiale, in modo da riuscire a sommare il tutto
                .Select(prestazioneGrouped => new PrestazionePrevista(prestazioneGrouped.Key, prestazioneGrouped.Sum(m => m.Tempo)));

            var righePrestazioni = prestazioniSegnate.Select(prestazioneSegnata =>
            {
                List<Cell> rigaPrestazione = new List<Cell>();
                Cell cellaDescrizione = new Cell(1, 1);
                cellaDescrizione.Add(new Paragraph(prestazioneSegnata.tipoContratto.Nome));
                cellaDescrizione.SetTextAlignment(TextAlignment.LEFT);
                cellaDescrizione.SetPaddingLeft(10);
                rigaPrestazione.Add(cellaDescrizione);

                Cell cellaTempo = new Cell(1, 1);
                tempoTotale += prestazioneSegnata.minuti;
                cellaTempo.Add(new Paragraph(TimeSpan.FromMinutes(prestazioneSegnata.minuti).OreMinutiFormattate()));
                cellaTempo.SetTextAlignment(TextAlignment.RIGHT);
                cellaTempo.SetPaddingRight(5);
                rigaPrestazione.Add(cellaTempo);

                if (reportOptions.mostraImporti)
                {
                    importoTotale += prestazioneSegnata.calcolaPaga();
                    Cell cellaPrezzo = new Cell(1, 1);
                    cellaPrezzo.Add(new Paragraph(Utilities.GetImportoCurrency(prestazioneSegnata.calcolaPaga())));
                    cellaPrezzo.SetTextAlignment(TextAlignment.RIGHT);
                    cellaPrezzo.SetPaddingRight(5);
                    rigaPrestazione.Add(cellaPrezzo);
                }
                return rigaPrestazione;
            }).SelectMany(x => x).ToList(); // SelectMany === flatten

            tabellaPrestazioni.AddCell(HeaderIntestazione);
            tabellaPrestazioni.AddCell(HeaderDescrizione);
            tabellaPrestazioni.AddCell(HeaderTempo);
            if (reportOptions.mostraImporti)
                tabellaPrestazioni.AddCell(headerCosto);

            // AGGIUNTA RIGA PRESTAZIONI (O RIGA VUOTA)
            if(righePrestazioni.ToList().Count >= 1)
                righePrestazioni.ToList().ForEach(cella => tabellaPrestazioni.AddCell(cella));
            else
            {
                Cell cellaVuota = new Cell(1, 3);
                cellaVuota.SetTextAlignment(TextAlignment.CENTER);
                cellaVuota.SetItalic();
                cellaVuota.Add(new Paragraph("Nessuna manodopera da visualizzare"));
                tabellaPrestazioni.AddCell(cellaVuota);
            }

            Cell FooterTOTALE = new Cell(1, 1);
            FooterTOTALE.SetTextAlignment(TextAlignment.CENTER);
            FooterTOTALE.SetBackgroundColor(backgroundHeader);
            FooterTOTALE.SetBold();
            FooterTOTALE.Add(new Paragraph("TOTALE"));

            Cell FooterTOTALETempo = new Cell(1, 1);
            FooterTOTALETempo.SetTextAlignment(TextAlignment.RIGHT);
            FooterTOTALETempo.Add(new Paragraph(TimeSpan.FromMinutes(Convert.ToDouble(tempoTotale)).OreMinutiFormattate()));
            FooterTOTALETempo.SetBold();
            FooterTOTALETempo.SetBackgroundColor(backgroundHeader);
            FooterTOTALETempo.SetPaddingRight(5);

            Cell FooterTOTALEImporto = new Cell(1, 1);
            FooterTOTALEImporto.SetTextAlignment(TextAlignment.RIGHT);
            FooterTOTALEImporto.Add(new Paragraph(Utilities.GetImportoCurrency(importoTotale)));
            FooterTOTALEImporto.SetBold();
            FooterTOTALEImporto.SetBackgroundColor(backgroundHeader);
            FooterTOTALEImporto.SetPaddingRight(5);

            tabellaPrestazioni.AddCell(FooterTOTALE);
            tabellaPrestazioni.AddCell(FooterTOTALETempo);
            if (reportOptions.mostraImporti)
                tabellaPrestazioni.AddCell(FooterTOTALEImporto);

            tabellaPrestazioni.AddHeaderCell(new Cell(1, 3)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetItalic().
                SetFontSize(10).
                Add(new Paragraph("Continua da pagina precedente...")));
            tabellaPrestazioni.SetSkipFirstHeader(true);
            return tabellaPrestazioni;
        }

        /// <summary>
        /// Metodo che costruisce una tabella generale dei totali
        /// </summary>
        /// <returns></returns>
        private Table BuildTabellaTotali(List<Lavoro> lavori, ReportOptions reportOptions)
        {
            // Table
            Table tabellaTotali = new Table(new float[] { 70, 100 });
            tabellaTotali.SetBorder(Border.NO_BORDER);
            tabellaTotali.SetHorizontalAlignment(HorizontalAlignment.RIGHT);

            Cell HeaderIntestazione = new Cell(1, 2)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetBold()
               .SetFontSize(13)
               .SetBorder(Border.NO_BORDER)
               .SetPaddingTop(15)
               .SetPaddingBottom(15)
               .Add(new Paragraph("Totale"));
            Cell Materiale = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .SetBorder(Border.NO_BORDER)
               .Add(new Paragraph("Materiale"));
            Cell totaleMateriale = new Cell(1,1)
               .SetTextAlignment(TextAlignment.RIGHT)
               .SetBorder(Border.NO_BORDER)
               .Add(new Paragraph(Utilities.GetImportoCurrency(lavori.Sum(lavoro => lavoro.calcolaTotaleMateriali()))));
            
            Cell Manodopera = new Cell(1, 1)
                .SetTextAlignment(TextAlignment.LEFT)
                .SetBorder(Border.NO_BORDER)
                .Add(new Paragraph("Manodopera"));
            Cell totaleManodopera = new Cell(1, 1)
                .SetTextAlignment(TextAlignment.RIGHT)
                .SetBorder(Border.NO_BORDER)
                .Add(new Paragraph(Utilities.GetImportoCurrency(lavori.Sum(lavoro => lavoro.calcolaTotalePrestazioni()))));

            // calcoliamo il totale comprensivo di manodopera se così è stato richiesto, altrimenti solo dei materiali
            decimal totale = reportOptions.mostraManodopera ? lavori.Sum(lavoro => lavoro.calcolaTotale()) : lavori.Sum(lavoro => lavoro.calcolaTotaleMateriali());
            Cell cellaVuota = new Cell(1, 1)
               .SetBorder(Border.NO_BORDER)
               .Add(new Paragraph(""));
            Cell cellaTotale = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.RIGHT)
               .SetBold()
               .SetBorder(Border.NO_BORDER)
               .Add(new Paragraph(Utilities.GetImportoCurrency(totale)));

            tabellaTotali.AddCell(HeaderIntestazione);
            tabellaTotali.AddCell(Materiale);
            tabellaTotali.AddCell(totaleMateriale);

            // nascondiamo la manodopera se così è stato richiesto dalle impostazioni
            if (reportOptions.mostraManodopera)
            {
                tabellaTotali.AddCell(Manodopera);
                tabellaTotali.AddCell(totaleManodopera);
            }
            tabellaTotali.AddCell(cellaVuota);
            tabellaTotali.AddCell(cellaTotale);
            tabellaTotali.SetKeepTogether(true);
            return tabellaTotali;
        }

        private Table BuildTableDescrizioneLavoro(Lavoro lavoro)
        {
            // Table
            Table tabellaDescrizioneLavoro = new Table(2);
            tabellaDescrizioneLavoro.SetHorizontalAlignment(HorizontalAlignment.RIGHT);
            tabellaDescrizioneLavoro.SetWidth(UnitValue.CreatePercentValue(40));
            tabellaDescrizioneLavoro.SetBorder(Border.NO_BORDER);

            List<Cell> cells = new List<Cell>();
            cells.Add(new Cell(1, 1)
                .SetWidth(70)
                .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Data")));
            cells.Add(new Cell(1, 1)
                .SetFontSize(9)
                .SetPaddingTop(5)
                .SetTextAlignment(TextAlignment.RIGHT)
               .Add(new Paragraph(lavoro.getData().ToString("dd/MM/yyyy"))));

            cells.Add(new Cell(1, 1)
                .SetWidth(70)
                .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Tipologia")));
            cells.Add(new Cell(1, 1)
                .SetFontSize(9)
                .SetPaddingTop(5)
                .SetTextAlignment(TextAlignment.RIGHT)
               .Add(new Paragraph(lavoro.IdTipologiaNavigation?.Descrizione ?? "")));
            
            cells.Add(new Cell(1, 1)
                .SetWidth(70)
                .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Indirizzo")));
            cells.Add(new Cell(1, 1)
                .SetFontSize(9)
                .SetPaddingTop(5)
                .SetTextAlignment(TextAlignment.RIGHT)
               .Add(new Paragraph(lavoro.Indirizzo ?? "")));

            cells.Add(new Cell(1, 1)
                .SetWidth(70)
                .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Referente")));
            cells.Add(new Cell(1, 1)
                .SetFontSize(9)
                .SetPaddingTop(5)
                .SetTextAlignment(TextAlignment.RIGHT)
               .Add(new Paragraph(lavoro.Referente ?? "")));

            cells.Add(new Cell(1, 1)
                .SetWidth(70)
                .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Note")));
            cells.Add(new Cell(1, 1)
                .SetFontSize(9)
                .SetPaddingTop(5)
                .SetTextAlignment(TextAlignment.RIGHT)
               .Add(new Paragraph(lavoro.Note ?? "")));

            cells.ForEach(cella => { cella.SetBorder(Border.NO_BORDER); cella.SetMaxWidth(400); });
            cells.ForEach(cella => tabellaDescrizioneLavoro.AddCell(cella));
            tabellaDescrizioneLavoro.SetKeepTogether(true);
            return tabellaDescrizioneLavoro;
        }
    }

    public class ReportOptions
    {
        public bool mostraImporti { get; set; }
        public bool mostraManodopera { get; set; }
        public bool unisci { get; set; }
        public TipologiaLavoro filtroTipologia { get; set; }
        public bool lavoriInFogliSeparati { get; set; }
    }
}
