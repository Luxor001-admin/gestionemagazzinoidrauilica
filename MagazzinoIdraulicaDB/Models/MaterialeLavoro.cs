﻿using shortid;
using shortid.Configuration;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class MaterialeLavoro
    {
        public MaterialeLavoro()
        {
            Id = Utilities.GenerateShotId();
        }
        public string Id { get; set; }
        public string IdMateriale { get; set; }
        public string IdLavoro { get; set; }
        private decimal _quantita;
        public decimal Quantita
        {
            get
            {
                return this._quantita;
            }
            set 
            {
                this._quantita = Convert.ToDecimal(String.Format("{0:0.00}", value));
            }
        }

        public virtual Lavoro IdLavoroNavigation { get; set; }
        public virtual Materiale IdMaterialeNavigation { get; set; }
    }
}
