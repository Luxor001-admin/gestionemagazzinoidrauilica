﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using shortid;
using shortid.Configuration;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cantiere> Cantiere { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Lavoro> Lavoro { get; set; }
        public virtual DbSet<Materiale> Materiale { get; set; }
        public virtual DbSet<MaterialeLavoro> MaterialeLavoro { get; set; }
        public virtual DbSet<Prestazione> Prestazione { get; set; }
        public virtual DbSet<TipoContratto> TipoContratto { get; set; }
        public virtual DbSet<TipologiaLavoro> TipologiaLavoro { get; set; }
        public static GenerationOptions generationOptions = new GenerationOptions() { Length = 12, UseNumbers = true };
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite($"DataSource={path}\\db.db");
            }
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cantiere>(entity =>
            {
                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.IdCliente)
                    .IsRequired()
                    .HasColumnName("id_cliente");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Cantiere)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasIndex(e => e.Nome)
                    .IsUnique();

                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome");
            });

            modelBuilder.Entity<Lavoro>(entity =>
            {
                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data")
                    .HasColumnType("NUMERIC");

                entity.Property(e => e.IdCantiere)
                    .IsRequired()
                    .HasColumnName("id_cantiere");

                entity.Property(e => e.IdTipologia)
                    .HasColumnName("id_tipologia");

                entity.Property(e => e.Segnato)
                    .IsRequired()
                    .HasColumnName("Segnato")
                    .HasColumnType("tinyint");

                entity.Property(e => e.Indirizzo).HasColumnName("indirizzo");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Referente).HasColumnName("referente");

                entity.HasOne(d => d.IdCantiereNavigation)
                    .WithMany(p => p.Lavoro)
                    .HasForeignKey(d => d.IdCantiere)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.IdTipologiaNavigation)
                    .WithMany(p => p.Lavoro)
                    .HasForeignKey(d => d.IdTipologia)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Materiale>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));
                
                entity.Property(e => e.Abbreviazione).HasColumnName("abbreviazione");
                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasColumnName("descrizione");

                entity.Property(e => e.Prezzo).HasColumnName("prezzo");
            });

            modelBuilder.Entity<MaterialeLavoro>(entity =>
            {
                entity.ToTable("Materiale_lavoro");

                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.HasIndex(e => e.IdMateriale);

                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.IdLavoro)
                    .IsRequired()
                    .HasColumnName("id_lavoro");

                entity.Property(e => e.IdMateriale)
                    .HasColumnName("id_materiale");

                entity.Property(e => e.Quantita).HasColumnName("quantita");

                entity.HasOne(d => d.IdLavoroNavigation)
                    .WithMany(p => p.MaterialeLavoro)
                    .HasForeignKey(d => d.IdLavoro)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.IdMaterialeNavigation)
                    .WithMany(p => p.MaterialeLavoro)
                    .HasForeignKey(d => d.IdMateriale)
                    .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<Prestazione>(entity =>
            {
                entity.HasIndex(e => e.IdLavoro);

                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.IdContratto)
                    .IsRequired()
                    .HasColumnName("id_contratto");

                entity.Property(e => e.IdLavoro)
                    .IsRequired()
                    .HasColumnName("id_lavoro");

                entity.Property(e => e.Tempo).HasColumnName("tempo");

                entity.HasOne(d => d.IdContrattoNavigation)
                    .WithMany(p => p.Prestazione)
                    .HasForeignKey(d => d.IdContratto)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.IdLavoroNavigation)
                    .WithMany(p => p.Prestazione)
                    .HasForeignKey(d => d.IdLavoro)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<TipoContratto>(entity =>
            {
                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome");

                entity.Property(e => e.PagaOraria).HasColumnName("pagaOraria");
            });

            modelBuilder.Entity<TipologiaLavoro>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .IsUnique();

                entity.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValue(ShortId.Generate(generationOptions));

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasColumnName("descrizione");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        public bool Exists<T>(T entity) where T : class
        {
            return this.Set<T>().Local.Contains(entity);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        /// <summary>
        /// Effettua un backup giornaliero
        /// </summary>
        public void DoDailyBackup()
        {
            string rootPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            if (!Directory.Exists($"{rootPath}\\backups"))
                Directory.CreateDirectory($"{rootPath}\\backup");
            File.Copy($"{rootPath}\\db.db", $"{rootPath}\\backup\\db-{DateTime.Now.Day}-{DateTime.Now.Month}.db", true);
        }
    }
}
