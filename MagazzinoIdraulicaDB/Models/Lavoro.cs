﻿using shortid;
using shortid.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class Lavoro
    {
        public Lavoro()
        {
            MaterialeLavoro = new HashSet<MaterialeLavoro>();
            Prestazione = new HashSet<Prestazione>();
            Id = Utilities.GenerateShotId();
            Data = BitConverter.GetBytes(DateTime.Now.Ticks);
        }

        public string Id { get; set; }
        public byte[] Data { get; set; }
        public string Indirizzo { get; set; }
        public string Referente { get; set; }
        public string Note { get; set; }
        public string IdCantiere { get; set; }
        public string IdTipologia { get; set; }
        public bool Segnato { get; set; }

        public virtual Cantiere IdCantiereNavigation { get; set; }
        public virtual TipologiaLavoro IdTipologiaNavigation { get; set; }
        public virtual ICollection<MaterialeLavoro> MaterialeLavoro { get; set; }
        public virtual ICollection<Prestazione> Prestazione { get; set; }

        public decimal calcolaTotaleMateriali()
        {
            return this.MaterialeLavoro.Select(materialeLavoro =>
            {
                if (materialeLavoro.IdMaterialeNavigation == null)
                    return 0;
                return materialeLavoro.IdMaterialeNavigation.Prezzo * materialeLavoro.Quantita;
            }).Sum();
        }

        public decimal calcolaTotalePrestazioni()
        {
            return this.Prestazione.Select(prestazioneLavoro => prestazioneLavoro.calcolaPaga()).Sum();
        }

        public decimal calcolaTotale()
        {
            return this.calcolaTotaleMateriali() + this.calcolaTotalePrestazioni();
        }

        public DateTime getData()
        {
            return new DateTime(BitConverter.ToInt64(this.Data, 0));
        }
    }
}
