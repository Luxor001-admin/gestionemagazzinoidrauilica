﻿using shortid.Configuration;
using shortid;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Cantiere = new HashSet<Cantiere>();
            Id = Utilities.GenerateShotId();
        }
        public Cliente(string nome)
        {
            Cantiere = new HashSet<Cantiere>();
            this.Nome = nome;
            Id = Utilities.GenerateShotId();
        }

        public string Id { get; set; }
        public string Nome { get; set; }

        public virtual ICollection<Cantiere> Cantiere { get; set; }
    }
}
