﻿using shortid;
using shortid.Configuration;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class TipoContratto
    {
        public TipoContratto()
        {
            Prestazione = new HashSet<Prestazione>();
            Id = Utilities.GenerateShotId();
        }

        public string Id { get; set; }
        public string Nome { get; set; }
        public decimal PagaOraria { get; set; }

        public virtual ICollection<Prestazione> Prestazione { get; set; }
    }
}
