﻿using shortid;
using shortid.Configuration;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class Materiale
    {
        public Materiale()
        {
            MaterialeLavoro = new HashSet<MaterialeLavoro>();
            Id = Utilities.GenerateShotId();
        }

        public string Id { get; set; }
        public string Descrizione { get; set; }
        public string Abbreviazione { get; set; }
        public decimal Prezzo { get; set; }

        public virtual ICollection<MaterialeLavoro> MaterialeLavoro { get; set; }
    }
}
