﻿using shortid.Configuration;
using shortid;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class Cantiere
    {
        public Cantiere()
        {
            Lavoro = new HashSet<Lavoro>();
            Id = Utilities.GenerateShotId();
        }
        public Cantiere(string nome, string idCliente)
        {
            Lavoro = new HashSet<Lavoro>();
            this.Nome = nome;
            this.IdCliente = idCliente;
            Id = Utilities.GenerateShotId();
        }

        public string Id { get; set; }
        public string Nome { get; set; }
        public string IdCliente { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual ICollection<Lavoro> Lavoro { get; set; }
    }
}
