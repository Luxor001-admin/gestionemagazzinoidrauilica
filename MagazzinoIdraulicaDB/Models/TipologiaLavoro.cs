﻿using shortid.Configuration;
using shortid;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class TipologiaLavoro
    {
        public TipologiaLavoro()
        {
            Lavoro = new HashSet<Lavoro>();
            Id = Utilities.GenerateShotId();
        }

        public string Id { get; set; }
        public string Descrizione { get; set; }

        public virtual ICollection<Lavoro> Lavoro { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}
