﻿using shortid;
using shortid.Configuration;
using System;
using System.Collections.Generic;

namespace MagazzinoIdraulicaDB.Models
{
    public partial class Prestazione
    {
        public Prestazione()
        {
            Id = Utilities.GenerateShotId();
        }
        public string Id { get; set; }
        public long Tempo { get; set; }
        public string IdLavoro { get; set; }
        public string IdContratto { get; set; }

        public virtual TipoContratto IdContrattoNavigation { get; set; }
        public virtual Lavoro IdLavoroNavigation { get; set; }

        public decimal calcolaPaga()
        {
            return (this.IdContrattoNavigation.PagaOraria / 60) * this.Tempo;
        }
    }
}
