﻿using shortid;
using shortid.Configuration;

namespace MagazzinoIdraulicaDB
{
    public static class Utilities
    {
        private static GenerationOptions generationOptions = new GenerationOptions() { Length = 12, UseNumbers = true, UseSpecialCharacters = true };
        public static string GenerateShotId()
        {
            return ShortId.Generate(generationOptions);
        }
    }
}
